package com.esinecan.thewasteland.constants;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by eren on 28.08.2016.
 */

/**
 * Statically accessed class that keeps the character types
 * and base stats.
 */
public class CharacterTypes {
    /**
     * Available types of characters. This is the place to edit
     * to create a new character.
     */
    public enum CharacterType{
        ROAD_WARRIOR, WAR_BOY, IMPERATOR, QUEST_GIVER, PIT_STOP_OWNER;

        /**
         * Subset of characters that are fighters. When a new fighter character
         * type is created, it needs to be added to here
         */
        public static EnumSet<CharacterType> FIGHTERS = EnumSet.of(ROAD_WARRIOR, WAR_BOY, IMPERATOR);
        /**
         * Base stats for fighter characters. When a new fighter type character is
         * created it needs to be added here as well.
         */
        public static final Map<CharacterType, CombatStats> FIGHTER_STATS;
        static
        {
            FIGHTER_STATS = new HashMap<CharacterType, CombatStats>();
            FIGHTER_STATS.put(ROAD_WARRIOR, new CombatStats(400, 200, 30));
            FIGHTER_STATS.put(WAR_BOY, new CombatStats(200, 150, 90));
            FIGHTER_STATS.put(IMPERATOR, new CombatStats(600, 100, 60));
        }
    }

    /**
     * Used in creation of enemies and tests.
     * @return a random fighter character type
     */
    public static CharacterType getRandomFighter(){
        int typeIndex = new Random().nextInt(CharacterType.FIGHTERS.size());
        CharacterType characterType = (CharacterType) CharacterType.FIGHTERS.toArray()[typeIndex];
        return characterType;
    }
}
