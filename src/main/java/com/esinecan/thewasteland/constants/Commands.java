package com.esinecan.thewasteland.constants;

import com.esinecan.thewasteland.Game;
import com.esinecan.thewasteland.character.FighterCharacter;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by eren on 30.08.2016.
 */

/**
 * The enum that any all commands are registered and mapped to their
 * appropriate functions.
 */
public enum Commands{
    MOVE, ATTACK, PITSTOP, QUEST, SAVE, INSPECT;

    /**
     * The map that maps commands to functions in the game class
     * Any new commands need to be registered here
     */
    public static final Map<Commands, Method> COMMANDS_METHOD_MAP;
    static {
        COMMANDS_METHOD_MAP = new HashMap<>();
        AddCommand(MOVE, "move");
        AddCommand(ATTACK, "attack");
        AddCommand(PITSTOP, "doPitStop");
        AddCommand(QUEST, "quest");
        AddCommand(INSPECT, "inspect");
    }

    /**
     * Helper method to Add a new command to the map
     * @param command command of type Commands
     * @param methodName name of the method in Game class
     */
    private static void AddCommand(Commands command, String methodName) {
        try {
            COMMANDS_METHOD_MAP.put(command, Game.class.getMethod(methodName, Constants.Direction.class, FighterCharacter.class));
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }
}