package com.esinecan.thewasteland.constants;

/**
 * Created by eren on 29.08.2016.
 */

/**
 * An intermediary class to keep base stats of fighter
 * type characters. Referenced in Character Types
 */
public class CombatStats {
    /**
     * Sets health, speed and damage for a character type
     * @param health
     * @param speed
     * @param damage
     */
    public CombatStats(int health, int speed, int damage){
        this.setHealth(health);
        this.setSpeed(speed);
        this.setDamage(damage);
    }

    /**
     * Hit points of the character's car
     */
    private int health;
    /**
     * Multiplied with 0.15, gives out the number of actions character can take per turn
     */
    private int speed;
    /**
     * Damage per hit
     */
    private int damage;

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }
}
