package com.esinecan.thewasteland.constants;

/**
 * Created by eren on 28.08.2016.
 */

/**
 * Class that contains all game parameters
 */
public class Constants {

    /**
     * This class needs to be accessed statically
     * Therefore we restirct instantiation with a private
     * constructor.
     */
    private Constants() {
        // to restrict instantiation
    }

    //MAP CONSTANTS
    /**
     * How many horizontal squares will the map have
     */
    public static final int MAP_WIDTH = 30;
    /**
     * How many vertical squares will the map have
     */
    public static final  int MAP_HEIGHT = 30;

    //NPC CONSTANTS
    /**
     * How many enemies will the map have upon game start.
     */
    public static final int INITIAL_ENEMY_COUNT = 10;
    /**
     * The distance enemies will notice and start
     * chasing you.
     */
    public static final int ENEMY_ATTACK_DISTANCE = 10;
    /**
     * How many healing pit stop points will the map
     * have when the game is started
     */
    public static final int INITIAL_PIT_STOP_COUNT = 10;
    /**
     * How many quest characters will the game have when started
     */
    public static final int INITIAL_QUEST_COUNT = 10;
    /**
     * We need to create new enemies per turn to offset
     * for the ones player kills. This is how many of them
     * are created per turn.
     */
    public static final int ENEMIES_CREATED_PER_TURN = 1;
    /**
     * Quest characters need to be created at a slower rate
     * than enemies. This is how many of them are created
     * every 5 turns.
     */
    public static final int QUESTS_CREATED_PER_FIVE_TURNS = 1;
    /**
     * This is a fun name pool enemies are named from
     */
    public static final String[] NPC_NAME_POOL = {"Dog", "Ace", "Aunty Entity", "Barry", "Bearclaw Mohawk", "Benno", "Big Rebecca", "Blackfinger", "Bubba Zanetti", "The Bullet Farmer", "Capable", "Captain Walker", "Charlie", "Cheedo the Fragile", "Clunk", "Coma-Doof Warrior", "Corpus Colossus", "Crow Fishers", "Cundalini", "Curmudgeon", "The Dag", "Dr. Dealgood", "Feral Kid", "Fifi Macaffee", "Glory the Child", "Grinner", "Gutgash", "Gyro Captain", "Immortan Joe", "Furiosa", "Ironbar", "Jedediah", "Jedediah Jr.", "Jessie Rockatansky", "Jim Goose", "Johnny the Boy", "Keeper of the Seeds", "Labatouche", "Lair", "Lord Humungus", "Master Blaster", "Max's monkey", "May Swaisey", "Mechanic Assistant", "Miss Giddy", "Morsov", "Mudguts", "Nathan", "Nightrider's Girl", "Nux", "The Organic Mechanic", "Pappagallo", "The People Eater", "Pig Killer", "Rictus Erectus", "Roop", "Sarse", "Savannah Nix", "Scrooloose", "Scuttle", "Silvertongue", "Slake", "Slit", "Sprog", "Starbuck", "Tenderloin", "The Captain's Girl", "The Collector", "The Golden Youth", "The Mechanic", "The Nightrider", "The Station Master", "The Toadie", "Toast the Knowing", "Toecutter", "Ton Ton Tattoo", "The Valkyrie", "Wez", "Zetta", "Ziggy", "Stonewall", "Cloak", "Books", "Plasma", "Wicked", "Blood", "Elsewhere", "Rat", "Mist", "Cyclops", "Abeline", "Accountant", "Ace", "Admin", "Alabaster", "Ambassador", "Ape", "Applejack", "Arizona", "Arrow", "Avenger", "Banjo", "Barbarian", "Bazooka", "Beans", "BeBop", "Beep", "Beezer", "Bingo", "Biscuit", "Blackjack", "Blackstone", "Blacktop", "Blade", "Blaze", "Blitz", "Blood", "Blooper", "Blowout", "Blowtorch", "Bluenote", "Blur", "Boffo", "Bolt", "Bomber", "Bombshell", "Bones", "Boogie", "Boots", "Brain", "Breaker", "Breezy", "Brig", "Brimstone", "Briny", "Brit", "Broker", "Bronco", "Broom", "Bruiser", "Buccaneer", "Buckhorn", "Bucktail", "Buffalo", "Bulldog", "Bulldozer", "Bumper", "Bunker", "Bupp", "Burgundy", "Butcher", "Butterfly", "Butterwing", "Buzzard", "Caboodle", "Cactus", "Calico", "Calypso", "Camel", "Cameroon", "Cannibal", "Cap", "Capricorn", "Carve", "Chameleon", "Chancellor", "Chantilly", "Chestnut", "Chevy", "Cheyenne", "Chiffon", "Chigger", "Chili Dog", "Chime", "Chopper", "Chowder", "Chute", "Cinnamon", "Clam", "Clarion", "Claw", "Cleaver", "Clipper", "Clopper", "Coach", "Cobalt", "Cobra", "Cockpit", "Colt", "Comp", "Coo-coo", "Coosh", "Corkscrew", "Cornbread", "Corncob", "Cottonfoot", "Crableg", "Crackers", "Crispy", "Crooner", "Crow", "Crumbs", "Crush", "Crusher", "Cupcake", "Cutter", "Cyclone", "Cymbal", "Dago", "Dakota", "Dambi", "Dancer", "Darkwing", "Dazz", "Demon", "Devil", "Dewdrop", "Diamondback", "Digger", "Diggity", "Ditto", "Ditty", "Dock", "Doggie", "Dong", "DooDah", "Downbeat", "Drag", "Dragon", "Driller", "Drummer", "Duck", "Dugout", "Duke", "Dumper", "Durango", "Dusky", "Dustwind", "Dutch", "Ember", "Emperor", "Fatso", "Fin", "Fingers", "Fireball", "Firebolt", "Fist", "Fixer", "Fizz", "Fleece", "Flook", "Foto", "Frog", "Fumbles", "Fungo", "Furf", "G-String", "Gash", "Giddyup", "Glaze", "Glider", "Glory", "Gog", "Gong", "Gonzo", "Gooch", "Goose", "Gossamer", "Graywing", "Greaser", "Grip", "Grizzly", "Groot", "Gumdrop", "Gunboat", "Gypsy", "Hammer", "Hangman", "Hatch", "Hatchback", "Hawse", "Hi-fi", "Hiccup", "Hickory", "Hippity", "Hojo", "Homestretch", "Honk", "Hoochee", "Hoodoo", "Hoofer", "Hoop", "Hot Rod", "Hub Cap", "Hummer", "Hush", "Hut-hut", "Iceman", "Iggy", "Imoo", "Indigo", "Iron Man", "Jackal", "Jade", "Jangle", "Jart", "Java", "Jawbreaker", "Jazz", "Jeep", "Jiffy", "Jigger", "Jigsaw", "Jingle", "Jinx", "Jipper", "Jitterbug", "Jive", "Jock", "Jolt", "Juju", "Juke Box", "Jumbiliah", "Jumper", "Jupiter", "K.O.", "Kickapoo", "Kicker", "Kiki", "King", "Kingmaker", "Kiwi", "Knuckleface", "Knucklenose", "Knuckles", "Koko", "Krone", "Lava", "Lefty", "Legs", "Lightning", "Limey", "Little Bit", "LoBall", "Lollipop", "Loo", "Lugnut", "Lunchbox", "Madiera", "Magnificent", "Mahogany", "Man-Eater", "Marigold", "Masher", "Mayday", "Meatball", "Meatloaf", "Melody", "Mex", "Misk", "Mistral", "Misty", "Mog", "Mojo", "Mombo", "Montana", "Moonbeam", "Moonglow", "Mootzie", "Muddy", "Munchy", "Mungo", "Nibbles", "Nighthorse", "Noodles", "Nuff", "Oogie", "Orchid", "Paffy", "Palamar", "Panky", "Peekaboo", "Piccolo", "Pick-off", "Pickles", "Pidge", "Piff", "Ping-Pong", "Pirate", "Pitchfork", "Pojo", "Poke", "Poof", "Pop-up", "Popcorn", "Potluck", "Potsie", "Poucho", "Pretzel", "Princess", "Pucker", "Puddin'", "Puff", "Pumper", "Quick", "Racer", "Raindrop", "Ram", "Rambler", "Rango", "Rasco", "Raven", "Razzle", "Rib Eye", "Rigger", "Rings", "Ringside", "Rink", "Ripsaw", "Rivet", "Rizzy", "Rocket", "Roo", "Rub-a-Dub", "Rudder", "Runway", "Sable", "Saddlebags", "Sahara", "Sapphire", "Sarge", "Sark", "Sash", "Sax", "Scar", "Schooner", "Scooter", "Screech", "Scud", "Scuff", "Scull", "Scuttlebutt", "Sea Horse", "Seaweed", "Sess", "Sha-Zam", "Shadrack", "Shaker", "Shalimar", "Shark", "Shepherd", "Shimmy", "Shine", "Shingle", "Shiver", "Shoke", "Shoo", "Shooter", "Shortcake", "Shot-Put", "Shutout", "Sidepockets", "Silky", "Silverado", "Silverleaf", "Sixpenny", "Sizzle", "Skadoo", "Skat", "Skater", "Skib", "Skid", "Skiff", "Skink", "Skull", "Skunk", "Slade", "Slappy", "Slash", "Sledge", "Slewfoot", "Slicer", "Slick", "Slider", "Slingo", "Sloo", "Sloop", "Slugger", "Smack", "Smash", "Smasher", "Smoke", "Snail", "Snake", "Snapper", "Snatch", "Snatcher", "Sneezer", "Sniper", "Snook", "Snoot", "Snort", "Snot", "Snout", "Snow", "Snowball", "Solo", "Sonar", "Sonic", "Soupy", "Spade", "Sparerib", "Spars", "Spike", "Spinner", "Spitter", "Spoiler", "Spokes", "Spook", "Spoons", "Spree", "Sprinkle", "Spud", "Spurlock", "Sputz", "Squint", "Stabber", "Steamboat", "Sticks", "Stinger", "Stone", "Strangler", "Striker", "Suitcase", "Surfboard", "Swede", "Swish", "Switchblade", "Swoop", "Swoosh", "Swoozie", "Taco", "Tailspin", "Tangerine", "Tango", "Tank", "Tanker", "Tape Deck", "Taps", "Tar Pit", "Tarf", "Tass", "Tater", "Tawny", "Tee-Shot", "Tempo", "Ten Pin", "Thunder", "Tin Pan", "Ting", "Toothpick", "Torch", "Torpedo", "Touchdown", "Trench", "Trig", "Trinity", "Troon", "Trotter", "Trucker", "Tunes", "Turk", "Tweedledee", "Tweedledum", "Vista", "Vulture", "Warrior", "Wart", "Weasel", "Weez", "Wharf", "Wheels", "Whiskey", "Whisper", "Whistler", "Whitehorn", "Whizzer", "Windjammer", "Wings", "Wishbone", "X-Men", "Yago", "Yahoo", "Yank", "Yaz", "Yellowhammer", "Yellowstone", "Yo-yo", "Yodel", "Yogo", "Yoho", "Yorky", "Zang", "Zap", "Zart", "Zazz", "Zephyr", "Ziggy", "Zing", "Zinger", "Zip", "Zoar", "Zook", "Zookie", "Zoot", "Zuzu"};
    /**
     * How many gold will a character have upon creation
     */
    public static final int BASE_GOLD = 300;
    /**
     * How many kills can a quest npc ask you to kill at most
     */
    public static final int MAX_KILLS_ASKED_BY_QUEST = 5;
    /**
     * Maximum amount of gold that can be earned from a quest
     */
    public static final int MAX_REWARDS_EARNED_BY_QUEST = 200;
    /**
     * Minimum amount of gold that can be earned from a quest
     */
    public static final int MIN_REWARDS_EARNED_BY_QUEST = 50;
    /**
     * Maximum cost a pit stop can have for healing
     */
    public static final int MAX_PIT_STOP_COST = 100;
    /**
     * Minimum cost a pit stop can have for healing
     */
    public static final int MIN_PIT_STOP_COST = 30;
    /**
     * Maximum hit points a pit stop can heal
     */
    public static final int MAX_PIT_STOP_HEAL = 100;
    /**
     * Minimum hit points a pit stop can heal
     */
    public static final int MIN_PIT_STOP_HEAL = 30;

    //PLAYER CONSTANTS
    /**
     * The multiplier speed is multiplied with to determine how
     * many actions can be take per turn
     */
    public static double SPEED_MODIFIER = 0.01;

    //LEVELING UP CONSTANTS
    /**
     * How much xp is earned per killed enemy
     */
    public static final int XP_PER_ENEMY = 20;
    /**
     * How much gold is earned per killed enemy
     */
    public static final int GOLD_PER_ENEMY = 20;
    /**
     * How much xp is earned per completed quest
     */
    public static final int XP_PER_QUEST = 50;
    /**
     * How much xp is required to level up
     */
    public static final int XP_REQUIRED_PER_LEVEL = 100;
    /**
     * How much stat points earned per level up
     */
    public static final int SKILL_POINTS_EARNED_PER_LEVEL = 30;

    //IN GAME ENUMS

    /**
     * The directions that can be moved towards
     */
    public enum Direction {
        NORTH, SOUTH, WEST, EAST
    }

    /**
     * Fighter character stats
     */
    public enum Stat {
        HEALTH, DAMAGE, SPEED
    }
}
