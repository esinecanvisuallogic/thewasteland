package com.esinecan.thewasteland;

import com.esinecan.thewasteland.character.Character;
import com.esinecan.thewasteland.character.*;
import com.esinecan.thewasteland.constants.CharacterTypes;
import com.esinecan.thewasteland.constants.Constants;
import com.esinecan.thewasteland.map.Coordinate;
import com.esinecan.thewasteland.map.CoordinateCalculator;
import com.esinecan.thewasteland.map.GameMap;
import com.esinecan.thewasteland.ui.FancyPrinter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by eren on 28.08.2016.
 */
public class Game implements Serializable{

    /**
     * Player's character
     */
    private PlayableCharacter playableCharacter;

    /**
     * Dynamic list of quests available on the map
     */
    public List<QuestGiverCharacter> questGivers;

    /**
     * Dynamic list of enemies available on the map
     */
    private List<EnemyCharacter> enemies;

    /**
     * Dynamic list of pit stops available on the map
     */
    private List<PitStopCharacter> pitStops;

    /**
     * Map of the current game with characters on it
     */
    private GameMap gameMap;

    /**
     * The method that starts the game. Creates a player
     * character based on the characterType, creates
     * enemies, pit stops and quests as specified in constants,
     * creates a map and randomly populates it with all these characters
     * @param characterType type of the player character
     */
    public void startGame(CharacterTypes.CharacterType characterType){
        createPlayableCharacter(characterType);
        CreateRandomEnemies();
        CreateQuestGivers();
        CreatePitStops();
        gameMap = new GameMap();

        gameMap.positionSingleCharacter(playableCharacter);
        gameMap.positionCharacters(enemies);
        gameMap.positionCharacters(pitStops);
        gameMap.positionCharacters(questGivers);
    }

    /**
     * performs a quest action in the given direction.
     * If the quest is not taken yet, marks it as started
     * and starts the quest's kill counter. If the quest
     * requirements are already completed, it rewards the
     * player and vanishes the quest from the map
     * @param direction
     * @param fighterCharacter
     * @return whether the action was successful
     */
    public boolean quest(Constants.Direction direction, FighterCharacter fighterCharacter) {
        boolean hasQuest = false;
        Coordinate coordinate = CoordinateCalculator.getCoordinateToDirection(direction, playableCharacter.getXPosition(), playableCharacter.getYPosition());
        Character character = CoordinateCalculator.getCharacterAtCoordinate(coordinate, gameMap);
        if(character!= null && character instanceof QuestGiverCharacter){
            QuestGiverCharacter questGiverCharacter = (QuestGiverCharacter) character;
            if(!questGiverCharacter.isQuestTaken() && !questGiverCharacter.isQuestComplete()){
                FancyPrinter.printQuestMessage(questGiverCharacter.getNumberToKill(), questGiverCharacter.getTypeToKill(), questGiverCharacter.getReward());
                questGiverCharacter.setQuestTaken(true);
                playableCharacter.useAction();
                hasQuest = true;
            }else if(questGiverCharacter.isQuestTaken() && questGiverCharacter.isQuestComplete()){
                questGivers.remove(questGiverCharacter);
                gameMap.getGameMap()[coordinate.y][coordinate.x] = null;
                playableCharacter.setGold(playableCharacter.getGold() + questGiverCharacter.getReward());
                playableCharacter.setExperience(playableCharacter.getExperience() + Constants.XP_PER_QUEST);
                FancyPrinter.printQuestRewardMessage(questGiverCharacter.getReward());
                playableCharacter.useAction();
                hasQuest = true;
            }
        }
        if(!hasQuest){
            FancyPrinter.displayInvalidActionMessage();
        }
        return hasQuest;
    }

    /**
     * Takes a direction and performs an attack action in the said
     * direction. if the said direction has an attackable character,
     * it reduces their health and spends an action point of the attacker.
     * if the attacked character is killed, and the attacker is a player
     * character, it awards the player with gold and xp. Also when the attacker
     * is a player character,
     * if the attacked character belongs to a character type from one
     * of the active quests, it increases the quest counter. if the quests requirements
     * are met, it marks the quest as complete.
     * @param direction
     * @param attackerCharacter
     * @return whether the action was successful
     */
    public boolean attack(Constants.Direction direction, FighterCharacter attackerCharacter) {
        boolean hasAttacked = false;
        Coordinate coordinate = CoordinateCalculator.getCoordinateToDirection(direction, attackerCharacter.getXPosition(), attackerCharacter.getYPosition());
        Character character = CoordinateCalculator.getCharacterAtCoordinate(coordinate, gameMap);
        if(character != null && character.isAttackable() && character instanceof FighterCharacter){
            FighterCharacter defenderCharacter = (FighterCharacter) character;
            defenderCharacter.setHealth(defenderCharacter.getHealth() - attackerCharacter.getDamage());
            playableCharacter.useAction();
            hasAttacked = true;
            if(defenderCharacter instanceof EnemyCharacter && attackerCharacter instanceof PlayableCharacter){
                FancyPrinter.printAttackMessage(((EnemyCharacter)defenderCharacter).getFullTitle(), defenderCharacter.getHealth());
            }
            if(defenderCharacter.getHealth() <= 0){
                if(attackerCharacter instanceof PlayableCharacter){
                    enemies.remove(defenderCharacter);
                    gameMap.getGameMap()[coordinate.y][coordinate.x] = null;
                    playableCharacter.setGold(playableCharacter.getGold() + Constants.GOLD_PER_ENEMY);
                    playableCharacter.setExperience(playableCharacter.getExperience() + Constants.XP_PER_ENEMY);
                    FancyPrinter.printKillMessage(((EnemyCharacter)defenderCharacter).getFullTitle());
                    for (QuestGiverCharacter questGiverCharacter :
                            questGivers) {
                        if(!questGiverCharacter.isQuestComplete() && questGiverCharacter.isQuestTaken() && questGiverCharacter.getTypeToKill() == defenderCharacter.getType()){
                            questGiverCharacter.setQuestCounter(questGiverCharacter.getQuestCounter() + 1);
                            if(questGiverCharacter.getQuestCounter() == questGiverCharacter.getNumberToKill()){
                                questGiverCharacter.setQuestComplete(true);
                                FancyPrinter.displayMessage(FancyPrinter.Messages.QUEST_COMPLETE);
                            }
                        }
                    }
                }
                else if(attackerCharacter instanceof EnemyCharacter && defenderCharacter instanceof PlayableCharacter){
                    FancyPrinter.printAttackWarning(((EnemyCharacter)attackerCharacter).getFullTitle());
                }
            }
        }else{
            if(attackerCharacter instanceof PlayableCharacter){
                FancyPrinter.displayInvalidActionMessage();
            }
        }
        return hasAttacked;
    }

    /**
     * Does a pit stop action in the given direction.
     * If there is a pit stop in that direction, and
     * the player is not at full health, and they have
     * enough gold, it heals the player for some gold.
     * @param direction
     * @param fighterCharacter
     * @return
     */
    public boolean doPitStop(Constants.Direction direction, FighterCharacter fighterCharacter) {
        PlayableCharacter playableCharacter = (PlayableCharacter)fighterCharacter;
        boolean hasHealed = false;
        Coordinate coordinate = CoordinateCalculator.getCoordinateToDirection(direction, playableCharacter.getXPosition(), playableCharacter.getYPosition());
        if(CoordinateCalculator.coordinatesValid(coordinate)){
            Character character = CoordinateCalculator.getCharacterAtCoordinate(coordinate, gameMap);
            if(character != null && character instanceof PitStopCharacter){
                PitStopCharacter pitStopCharacter =(PitStopCharacter) character;
                if(pitStopCharacter.getCost() <= playableCharacter.getGold() && playableCharacter.getFullHealth() > playableCharacter.getHealth()){
                    playableCharacter.setGold(playableCharacter.getGold() - pitStopCharacter.getCost());
                    int newHealth = playableCharacter.getHealth() + pitStopCharacter.getHealingCapacity();
                    if(newHealth > playableCharacter.getFullHealth()){
                        newHealth = playableCharacter.getFullHealth();
                    }
                    playableCharacter.setHealth(newHealth);
                    hasHealed = true;
                    playableCharacter.useAction();
                    FancyPrinter.printHealInfo(playableCharacter.getHealth(), playableCharacter.getFullHealth(), playableCharacter.getGold());
                }else if(pitStopCharacter.getCost() > playableCharacter.getGold()){
                    FancyPrinter.displayMessage(FancyPrinter.Messages.PIT_STOP_NOT_ENOUGH_GOLD);
                }else if(playableCharacter.getFullHealth() > playableCharacter.getHealth()){
                    FancyPrinter.displayMessage(FancyPrinter.Messages.PIT_STOP_ALREADY_FULL);
                }
            }
        }
        if(!hasHealed){
            FancyPrinter.displayInvalidActionMessage();
        }
        return hasHealed;
    }

    /**
     * Called when the player levels up,
     * it increases the level of the player
     * by one, and increases the given stat of the
     * player by SKILL_POINTS_EARNED_PER_LEVEL specified
     * in the constants.
     * @param stat
     */
    public void levelUp(Constants.Stat stat){
        if(stat == Constants.Stat.DAMAGE){
            playableCharacter.setDamage(playableCharacter.getDamage() + Constants.SKILL_POINTS_EARNED_PER_LEVEL);
        }else if(stat == Constants.Stat.HEALTH){
            playableCharacter.setHealth(playableCharacter.getHealth() + Constants.SKILL_POINTS_EARNED_PER_LEVEL);
            playableCharacter.setFullHealth(playableCharacter.getFullHealth() + Constants.SKILL_POINTS_EARNED_PER_LEVEL);
        }else if (stat == Constants.Stat.SPEED){
            playableCharacter.setSpeed(playableCharacter.getSpeed() + Constants.SKILL_POINTS_EARNED_PER_LEVEL);
        }
        playableCharacter.setLevel(playableCharacter.getLevel() + 1);
    }

    /**
     * Uses up action point of the enemy to perform
     * an automatic act. If the player is adjacent,
     * they'll attack the player. If the player
     * is closer than ENEMY_ATTACK_DISTANCE specified
     * in the Constants, they'll chase the player,
     * otherwise, they'll move randomly.
     * @param enemyCharacter
     * @return
     */
    public boolean moveEnemy(EnemyCharacter enemyCharacter) {
        int xDistance = Math.abs(enemyCharacter.getXPosition() - playableCharacter.getXPosition());
        int yDistance = Math.abs(enemyCharacter.getYPosition() - playableCharacter.getYPosition());
        if(xDistance == 1){
            if(enemyCharacter.getXPosition() > playableCharacter.getXPosition()){
                return attack(Constants.Direction.WEST, enemyCharacter);
            }else {
                return attack(Constants.Direction.EAST, enemyCharacter);
            }
        }else if(yDistance == 1){
            if(enemyCharacter.getYPosition() > playableCharacter.getYPosition()){
                return attack(Constants.Direction.NORTH, enemyCharacter);
            }else {
                return attack(Constants.Direction.SOUTH, enemyCharacter);
            }
        }
        double distance = Math.sqrt(Math.pow(xDistance, 2) + Math.pow(yDistance, 2));
        if(distance <= Constants.ENEMY_ATTACK_DISTANCE){
            Constants.Direction direction;
            if(xDistance > yDistance){
                if(playableCharacter.getXPosition() > enemyCharacter.getXPosition()){
                    direction = Constants.Direction.EAST;
                }else{
                    direction = Constants.Direction.WEST;
                }
            }else{
                if(playableCharacter.getYPosition() > enemyCharacter.getYPosition()){
                    direction = Constants.Direction.SOUTH;
                }else{
                    direction = Constants.Direction.NORTH;
                }
            }
            if(!move(direction,enemyCharacter)){
                return moveEnemyRandomly(enemyCharacter);
            }else{
                String announcement = enemyCharacter.getFullTitle() + " is chasing you!";
                System.out.println(announcement.replace("_"," "));
            }
        }else{
            return moveEnemyRandomly(enemyCharacter);
        }
        return false;
    }

    /**
     * Places a new EnemyCharacter of a random
     * type on the map on a random position.
     */
    public void addARandomEnemy() {
        CharacterTypes.CharacterType characterType = CharacterTypes.getRandomFighter();
        EnemyCharacter enemyCharacter = new EnemyCharacter(characterType);
        enemies.add(enemyCharacter);
        gameMap.positionSingleCharacter(enemyCharacter);
    }

    /**
     * Places a new QuestGiverCharacter of a random
     * type on the map on a random position.
     */
    public void addARandomQuest() {
        QuestGiverCharacter questGiverCharacter = new QuestGiverCharacter();
        questGivers.add(questGiverCharacter);
        gameMap.positionSingleCharacter(questGiverCharacter);
    }

    /**
     * Called by moveEnemy, it moves the enemy
     * to a random direction.
     * @param enemyCharacter
     * @return
     */
    public boolean moveEnemyRandomly(EnemyCharacter enemyCharacter){
        boolean hasMoved = false;
        while(!hasMoved){
            int pick = new Random().nextInt(Constants.Direction.values().length);
            Constants.Direction direction = Constants.Direction.values()[pick];
            hasMoved = move(direction, enemyCharacter);
        }
        return hasMoved;
    }

    /**
     * It tries to move the given character in the
     * given direction. If that position is already occupied,
     * returns false. Otherwise changes the position of the
     * character, uses up an action point of them, and returns
     * true.
     * @param direction
     * @param character
     * @return
     */
    public boolean move(Constants.Direction direction, FighterCharacter character){
        Coordinate coordinate = CoordinateCalculator.getCoordinateToDirection(direction, character.getXPosition(), character.getYPosition());
        if(!CoordinateCalculator.coordinatesValid(coordinate)){
            if(character instanceof PlayableCharacter){
                FancyPrinter.displayMessage(FancyPrinter.Messages.OUT_OF_BOUNDS);
            }
            return false;
        }else if(!CoordinateCalculator.isCoordinateEmpty(coordinate, gameMap)){
            if(character instanceof PlayableCharacter){
                FancyPrinter.displayMessage(FancyPrinter.Messages.ALREADY_OCCUPIED);
            }
            return false;
        }else {
            gameMap.getGameMap()[coordinate.y][coordinate.x] = character;
            gameMap.getGameMap()[character.getYPosition()][character.getXPosition()] = null;
            character.setXPosition(coordinate.x);
            character.setYPosition(coordinate.y);
            character.useAction();
            if(character instanceof PlayableCharacter){
                FancyPrinter.directionInfo(direction.name());
            }
            return true;
        }
    }

    /**
     * An action that does not consume any action points.
     * If there is a character in the given direction,
     * It displays their properties depending on their
     * type.
     * @param direction
     * @param character
     * @return
     */
    public boolean inspect(Constants.Direction direction, FighterCharacter character){
        Coordinate coordinate = CoordinateCalculator.getCoordinateToDirection(direction, character.getXPosition(), character.getYPosition());
        if(!CoordinateCalculator.coordinatesValid(coordinate)){
            FancyPrinter.displayMessage(FancyPrinter.Messages.OUT_OF_BOUNDS);
        }else if(CoordinateCalculator.isCoordinateEmpty(coordinate, gameMap)){
            FancyPrinter.displayMessage(FancyPrinter.Messages.EMPTY_SQUARE);
        }else {
            Character inspectedCharacter = CoordinateCalculator.getCharacterAtCoordinate(coordinate, gameMap);
            if(inspectedCharacter instanceof EnemyCharacter){
                FancyPrinter.printEnemyInfo((EnemyCharacter) inspectedCharacter);
            }else if(inspectedCharacter instanceof PitStopCharacter){
                FancyPrinter.printPitStopInfo(((PitStopCharacter) inspectedCharacter));
            }else if(inspectedCharacter instanceof QuestGiverCharacter){
                FancyPrinter.printQuestCharacterInfo((QuestGiverCharacter) inspectedCharacter);
            }
        }
        return false;
    }

    /**
     * Creates and initializes a new player character
     * of the given type
     * @param characterType
     */
    public void createPlayableCharacter(CharacterTypes.CharacterType characterType){
        this.playableCharacter = new PlayableCharacter(characterType);
    }

    /**
     * Creates INITIAL_ENEMY_COUNT (specified in Constants)
     * enemy characters of random types.
     */
    public void CreateRandomEnemies() {
        enemies = new ArrayList<>();
        for(int i = 0; i< Constants.INITIAL_ENEMY_COUNT; i++){
            CharacterTypes.CharacterType characterType = CharacterTypes.getRandomFighter();
            EnemyCharacter enemyCharacter = new EnemyCharacter(characterType);
            enemies.add(enemyCharacter);
        }
    }

    /**
     * Creates INITIAL_QUEST_COUNT (specified in Constants) characters
     * of the type QuestGiverCharacter
     */
    public void CreateQuestGivers() {
        questGivers = new ArrayList<>();
        for(int i = 0; i< Constants.INITIAL_QUEST_COUNT; i++){
            questGivers.add(new QuestGiverCharacter());
        }
    }

    /**
     * Creates INITIAL_PIT_STOP_COUNT (specified in Constants) characters
     * of the type PitStopCharacter
     */
    public void CreatePitStops() {
        pitStops = new ArrayList<>();
        for(int i = 0; i< Constants.INITIAL_PIT_STOP_COUNT; i++){
            pitStops.add(new PitStopCharacter());
        }
    }

    public void setEnemies(List<EnemyCharacter> enemies) {
        this.enemies = enemies;
    }

    public PlayableCharacter getPlayableCharacter() {
        return playableCharacter;
    }

    public List<EnemyCharacter> getEnemies() {
        return enemies;
    }

    public List<QuestGiverCharacter> getQuestGivers() {
        return questGivers;
    }

    public List<PitStopCharacter> getPitStops() {
        return pitStops;
    }

    public GameMap getGameMap() {
        return gameMap;
    }

    public void setGameMap(GameMap gameMap) {
        this.gameMap = gameMap;
    }

    public boolean hasPlayerLeveledUp() {
        return Math.floor(playableCharacter.getExperience() / Constants.XP_REQUIRED_PER_LEVEL) >= playableCharacter.getLevel();
    }
}
