package com.esinecan.thewasteland.io;

import com.esinecan.thewasteland.Game;
import com.esinecan.thewasteland.constants.CharacterTypes;
import jdk.nashorn.internal.runtime.regexp.joni.encoding.CharacterType;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Created by eren on 29.08.2016.
 */

/**
 * Class to save and load a game
 */
public class SaveAndLoad {
    /**
     * Saves the game. Does it by serializing
     * the game and all of its references,
     * and then outputting them to a file.
     * @param game current game object
     * @return is saving successful
     */
    public boolean save(Game game){
        try{

            FileOutputStream fout = new FileOutputStream("game.ser");
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(game);
            oos.close();
            return true;
        }catch(Exception ex){
            ex.printStackTrace();
            return false;
        }
    }

    /**
     * loads an existing game. reads the file that
     * contains the serialized game object and its
     * references, deserializes them, and returns them.
     * @return the game object to be loaded
     */
    public Game load(){
        try{
            Game game;
            FileInputStream fin = new FileInputStream("game.ser");
            ObjectInputStream ois = new ObjectInputStream(fin);
            game = (Game) ois.readObject();
            ois.close();

            return game;

        }catch(Exception ex){
            ex.printStackTrace();
            return null;
        }
    }
}
