package com.esinecan.thewasteland.ui;

/**
 * Created by eren on 29.08.2016.
 */

import com.esinecan.thewasteland.Game;
import com.esinecan.thewasteland.character.Character;
import com.esinecan.thewasteland.character.*;
import com.esinecan.thewasteland.constants.CharacterTypes;
import com.esinecan.thewasteland.constants.Commands;
import com.esinecan.thewasteland.constants.Constants;

/**
 * Statically accessed class for stylized command line printing
 */
public class FancyPrinter {
    /**
     * standart command line print color
     */
    public static final String ANSI_RESET = "\u001B[0m";
    /**
     * red command line print color
     */
    public static final String ANSI_RED = "\u001B[31m";
    /**
     * green command line print color
     */
    public static final String ANSI_GREEN = "\u001B[32m";
    /**
     * yellow command line print color
     */
    public static final String ANSI_YELLOW = "\u001B[33m";
    /**
     * blue command line print color
     */
    public static final String ANSI_BLUE = "\u001B[34m";
    /**
     * purple command line print color
     */
    public static final String ANSI_PURPLE = "\u001B[35m";
    /**
     * cyan command line print color
     */
    public static final String ANSI_CYAN = "\u001B[36m";
    /**
     * white command line print color
     */
    public static final String ANSI_WHITE = "\u001B[37m";

    /**
     * Called at the beginning of the game.
     * Greets users with an ascii art
     */
    public static void printWelcome(){
        System.out.println(ANSI_RED + "                                           _|                _|                            _|  ");
        System.out.println(" _|      _|      _|    _|_|_|    _|_|_|  _|_|_|_|    _|_|    _|    _|_|_|  _|_|_|      _|_|_|  ");
        System.out.println(" _|      _|      _|  _|    _|  _|_|        _|      _|_|_|_|  _|  _|    _|  _|    _|  _|    _|  ");
        System.out.println("   _|  _|  _|  _|    _|    _|      _|_|    _|      _|        _|  _|    _|  _|    _|  _|    _|  ");
        System.out.println("     _|      _|        _|_|_|  _|_|_|        _|_|    _|_|_|  _|    _|_|_|  _|    _|    _|_|_|" + ANSI_RESET);
    }

    /**
     * Prints some explanations about different Fighter Character types
     */
    public static void characterIntroduction(){
        System.out.println("Speed times 0.01 means the action number you can take in one turn");
        System.out.println("Health is your resilience to damage");
        System.out.println("Damage is how much hurt you can inflict on others");
        System.out.println("What character do you want?");
        System.out.println("Imperator drives a heavy truck: Damage(60), Health(600), Speed(100)");
        System.out.println("War Boy is a crazed berserker: Damage(90), Health(200), Speed(150)");
        System.out.println("Road Warrior is the quickest: Damage(30), Health(400), Speed(200)");
    }

    /**
     * Prints instructions on how to play the game
     */
    public static void PrintInstructions(){
        System.out.println("Welcome to wasteland! It is all downhill from here. Below is the map of the terrain");
        System.out.println("Now I'll show you the map. You are the one marked with Y");
        System.out.println("The ones marked with Red 'E's are enemies. They'll start chasing you if you come close");
        System.out.println("The ones marked with Purple 'P's are pit stops. They'll fix your car for some gold");
        System.out.println("The ones marked with 'Q' will give you quests. A little extra experience and cash");
        System.out.println("'Q's are yellow if you didn't take the quest, blue if you took them but did not complete, and green if you're ready to go back and collect");
    }

    /**
     * Prints examples of commands that can be used
     */
    public static void exempliGratia(){
        System.out.println("you have 4 actions. MOVE,ATTACK,QUEST and PITSTOP");
        System.out.println("Also 4 directions. NORTH, SOUTH, you know the rest");
        System.out.println("When you want to interact with something, go to an adjacent square");
        System.out.println("For example to attack an enemy, go to southward adjacent square of it and type");
        System.out.println("ATTACK NORTH");
        System.out.println("Now explore a little");
    }

    /**
     * Called when the player is dead.
     * Prints a stylized game over ascii art
     */
    public static void printGameOver(){
        System.out.println(ANSI_RED + "   _|_|_|    _|_|_|  _|_|_|  _|_|      _|_|          _|_|    _|      _|    _|_|    _|  _|_|  ");
        System.out.println(" _|    _|  _|    _|  _|    _|    _|  _|_|_|_|      _|    _|  _|      _|  _|_|_|_|  _|_|      ");
        System.out.println(" _|    _|  _|    _|  _|    _|    _|  _|            _|    _|    _|  _|    _|        _|        ");
        System.out.println("   _|_|_|    _|_|_|  _|    _|    _|    _|_|_|        _|_|        _|        _|_|_|  _|        ");
        System.out.println("       _|                                                                                    ");
        System.out.println("     _|_|" + ANSI_RESET);
    }


    //EREN: This could have been done so much better

    /**
     * Takes a message from the game object and prints it
     * @param s a message
     */
    public static void displayMessage(String s){
        System.out.println(s.replace("_", " "));
    }

    /**
     * prints the current state of the map.
     * prints enemies as red Esç
     * prints pit stops as purple Ps.
     * prints player as a cyan Y.
     * prints Quest givers as Qs,
     * paints them yellow if the quest is not taken,
     * blue if the quest is taken, but there is killing to do,
     * green if player is ready to report back and collect.
     * @param game
     */
    public static void printMap(Game game) {
        for(int i = 0; i< Constants.MAP_HEIGHT; i++){
            for(int j = 0;j< Constants.MAP_WIDTH; j++){
                Character c = game.getGameMap().getGameMap()[i][j];
                System.out.print("|");
                if( c == null){
                    System.out.print(" ");
                }else if(c instanceof PlayableCharacter){
                    System.out.print(ANSI_CYAN + "Y" + ANSI_RESET);
                }else if(c instanceof QuestGiverCharacter){
                    String ansiColor = ANSI_WHITE;
                    QuestGiverCharacter questGiverCharacter = (QuestGiverCharacter)c;
                    if(!questGiverCharacter.isQuestTaken() && !questGiverCharacter.isQuestComplete()){
                        ansiColor = ANSI_YELLOW;
                    }else if (questGiverCharacter.isQuestTaken() && !questGiverCharacter.isQuestComplete()){
                        ansiColor = ANSI_BLUE;
                    }else if (questGiverCharacter.isQuestTaken() && questGiverCharacter.isQuestComplete()){
                        ansiColor = ANSI_GREEN;
                    }
                    System.out.print(ansiColor + "Q" + ANSI_RESET);
                }else if(c instanceof EnemyCharacter){
                    System.out.print(ANSI_RED + "E" + ANSI_RESET);
                }else if(c instanceof PitStopCharacter){
                    System.out.print(ANSI_PURPLE + "P" + ANSI_RESET);
                }else{
                    System.out.print("X");
                }
                if(j==Constants.MAP_WIDTH-1){
                    System.out.print("|");
                }
            }
            System.out.println();
        }
        PlayableCharacter pc = game.getPlayableCharacter();
        System.out.println("Health:"+ pc.getHealth() + "/" + pc.getFullHealth() + " Damage:" + pc.getDamage() + " Speed:"+pc.getSpeed() + " Actions:"+pc.getRemainingActions() + " Gold:"+pc.getGold());
    }

    /**
     * prints the message asking the player to enter a command.
     * Dynamically lists the alternatives too
     */
    public static void printCommantInputMessage(){
        String commandIn = "What do you want to do? (";
        for(int i = 0; i< Commands.values().length; i++){
            commandIn+= Commands.values()[i];
            if(i<Commands.values().length - 1){
                commandIn+= ", ";
            }
        }
        commandIn += ")";
        displayMessage(commandIn);
    }

    /**
     * A quest giver's message to player
     * @param numToKill
     * @param characterTypeToKill
     * @param reward
     */
    public static void printQuestMessage(int numToKill, CharacterTypes.CharacterType characterTypeToKill, int reward){
        displayMessage("Hey, hunt me "+ numToKill + " of those " + characterTypeToKill + " types and you get " + reward + " golds and some XP");
    }

    /**
     * Prints message when user completes a quest
     * @param reward
     */
    public static void printQuestRewardMessage(int reward){
        displayMessage("You just earned yourself " + Constants.XP_PER_QUEST + " XP and " + reward + " pieces of gold.");
    }

    /**
     * Notifies user when attacked
     * @param title
     * @param health
     */
    public static void printAttackMessage(String title, int health){
        displayMessage("You attacked "+ title + " and they've got " + health + " health left");
    }

    /**
     * Notifies user when they kill an enemy
     * @param title
     */
    public static void printKillMessage(String title){
        displayMessage("You just coldly murdered " + title);
    }

    /**
     * Notifies user when they are attacked
     * @param title
     */
    public static void printAttackWarning(String title){
        displayMessage(title + " attacked you!");
    }

    /**
     * Displays the healing amount and the remaining gold
     * @param health
     * @param fullHealth
     * @param gold
     */
    public static void printHealInfo(int health, int fullHealth, int gold){
        displayMessage("There! good as new! (current health:" + health + "/" + fullHealth + ", gold:" + gold + ")");
    }

    /**
     * notifies user upon successful movement
     * @param direction
     */
    public static void directionInfo(String direction){
        displayMessage("You've moved one square to " + direction);
    }

    /**
     * Displays the stats of the enemy
     * @param enemyCharacter
     */
    public static void printEnemyInfo(EnemyCharacter enemyCharacter){
        displayMessage("You see " + enemyCharacter.getFullTitle() + "(Speed: " + enemyCharacter.getSpeed() + ", Damage: " + enemyCharacter.getDamage() + ", Health:" + enemyCharacter.getHealth() + ")");
    }

    /**
     * Displays the stats of the pit stop
     * @param pitStopCharacter
     */
    public static void printPitStopInfo(PitStopCharacter pitStopCharacter){
        displayMessage("A pit stop. They will restore " + pitStopCharacter.getHealingCapacity() + " health for " + pitStopCharacter.getCost() + " gold");
    }

    /**
     * Prints the state and stats of the quest giver
     * @param questGiverCharacter
     */
    public static void printQuestCharacterInfo(QuestGiverCharacter questGiverCharacter){
        if(questGiverCharacter.isQuestTaken()){
            if(!questGiverCharacter.isQuestComplete()){
                FancyPrinter.displayMessage("You're trying to kill " + questGiverCharacter.getNumberToKill() + " "+questGiverCharacter.getTypeToKill()+"s for " + questGiverCharacter.getReward() + " pieces of gold. (Currently Killed:" + questGiverCharacter.getQuestCounter() + ")");
            }else{
                FancyPrinter.displayMessage("You killed " + questGiverCharacter.getNumberToKill() + " "+questGiverCharacter.getTypeToKill()+"s for " + questGiverCharacter.getReward() + " pieces of gold");
                FancyPrinter.displayMessage("Go collect your reward champ. (use quest action)");
            }
        }else{
            FancyPrinter.displayMessage ("They are looking for someone to kill " + questGiverCharacter.getNumberToKill() + " "+questGiverCharacter.getTypeToKill()+"s for " + questGiverCharacter.getReward() + " pieces of gold");
        }
    }

    /**
     * Prints generic invalid action message
     */
    public static void displayInvalidActionMessage(){
        displayMessage("That is not a valid action");
    }
    /**
     * Prints generic invalid input message
     */
    public static void displayInvalidInputMessage(){
        displayMessage("Couldn't catch your meaning there champ.");
    }


    public static class Messages{
        public static final String NEW_GAME_OR_RESUME = "Do you want a new game or to resume one? (new/resume)";
        public static final String CHOOSE_CHARACTER = "Choose one by typing it's name";
        public static final String MAP_DISPLAY_APPROVAL = "you ready to see the map?";
        public static final String GAME_SAVED = "The game has been saved";
        public static final String LVL_UP = "You leveled up bub! What stat do you want to improve? (health/damage/speed)";
        public static final String YES_OR_NO = "Just say yes or no (y/n)";
        public static final String QUEST_COMPLETE = "You completed a quest. Go report";
        public static final String PIT_STOP_NOT_ENOUGH_GOLD = "Sorry chap, you don't have the money to do a pit stop";
        public static final String PIT_STOP_ALREADY_FULL = "You're already healthy as a horse!";
        public static final String OUT_OF_BOUNDS = "That location is not on the map";
        public static final String ALREADY_OCCUPIED = "That location is occupied";
        public static final String EMPTY_SQUARE = "There is nothing there";
    }
}
