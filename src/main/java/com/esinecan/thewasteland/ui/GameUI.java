package com.esinecan.thewasteland.ui;

import com.esinecan.thewasteland.Game;
import com.esinecan.thewasteland.character.EnemyCharacter;
import com.esinecan.thewasteland.character.PlayableCharacter;
import com.esinecan.thewasteland.constants.CharacterTypes;
import com.esinecan.thewasteland.constants.Commands;
import com.esinecan.thewasteland.constants.Constants;
import com.esinecan.thewasteland.io.SaveAndLoad;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static com.esinecan.thewasteland.ui.FancyPrinter.Messages;
import static com.esinecan.thewasteland.ui.FancyPrinter.displayInvalidInputMessage;

/**
 * Created by eren on 29.08.2016.
 */

/**
 * The class that deals with user interactions and calls game object's
 * methods to direct the game flow
 */
public class GameUI {

    /**
     * Buffered reader to take user input throughout the game
     */
    private static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    /**
     * A save and load object to load a game or save and exit the current one
     */
    private static SaveAndLoad saveAndLoad = new SaveAndLoad();

    /**
     * runs when the game is first opened. Asks user whether they want
     * to start a new game, or continue an existing game.
     */
    public static void start(){
        FancyPrinter.printWelcome();
        String input = "";
        while(!input.equals("new") && !input.equals("resume")){
            FancyPrinter.displayMessage(Messages.NEW_GAME_OR_RESUME);
            try {
                input = br.readLine();
                input = input.toLowerCase().trim();
            } catch (IOException e) {
                displayInvalidInputMessage();
            }
        }
        if(input.equals("resume")){
            loadGame();
        }else {
            newGame();
        }
    }

    /**
     * Called by start method if the user elects to load a game.
     * If there is no game to load, starts a new game.
     * If it can load a game, it will set it as the current game
     * object and proceed with it.
     */
    private static void loadGame(){
        Game game = saveAndLoad.load();
        if(game == null){
            newGame();
        }else{
            FancyPrinter.printMap(game);
            play(game);
        }
    }

    /**
     * Called by the start method if the user elects to start a new game.
     * It makes the user choose a character type at the beginning,
     * prints bunch of interactions, then initializes the game
     */
    private static void newGame(){
        Game game = new Game();
        FancyPrinter.characterIntroduction();
        String input = "";
        while(!validateCharacterType(input)){
            FancyPrinter.displayMessage(Messages.CHOOSE_CHARACTER);
            try {
                input = br.readLine();
                input = input.toLowerCase().trim();
            } catch (Exception e) {
                FancyPrinter.displayInvalidInputMessage();
            }
        }
        input = input.toUpperCase().replace(" ", "_");
        game.startGame(CharacterTypes.CharacterType.valueOf(input));
        FancyPrinter.PrintInstructions();
        FancyPrinter.displayMessage(Messages.MAP_DISPLAY_APPROVAL);
        if(yesNoInput()){
            FancyPrinter.printMap(game);
        }
        FancyPrinter.exempliGratia();
        play(game);
    }

    /**
     * returns true if the input is a valid character type
     * @param s character type string
     * @return
     */
    private static boolean validateCharacterType(String s){
        try{
            CharacterTypes.CharacterType.valueOf(s.replace(" ", "_").toUpperCase());
            return true;
        }catch (Exception e){
            return false;
        }
    }

    /**
     * Both the new game and the load game methods call this method
     * once they initialize a game object. This method contains the game
     * loop and remembers how many turns have been played.
     * @param game an initialized game object
     */
    private static void play(Game game){
        int howManyTurnsPassed=0;
        while (true){
            playNextTurn(game, howManyTurnsPassed);
            howManyTurnsPassed++;
        }
    }

    /**
     * This method handles a turn of the game. It starts
     * by taking user actions until the player runs
     * out of action points for that turn.
     * If the player levels up due to their actions during
     * the turn, it brings the level up dialogue.
     * Then it handles the enemy actions.
     * Finally, it creates new enemies and quests as
     * necessary.
     * @param game current game
     * @param turn turns passed since the game has started
     */
    private static void playNextTurn(Game game, int turn) {
        PlayableCharacter pc = game.getPlayableCharacter();
        if(pc.getHealth() <= 0){
            FancyPrinter.printGameOver();
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.exit(0);
        }
        pc.refreshActions();
        while (pc.getRemainingActions()>0){
            boolean validAction = false;
            while(!validAction){
                String[] input = actionInput();
                String command = input[0];
                if(Commands.valueOf(command) == Commands.SAVE){
                    saveAndLoad.save(game);
                    FancyPrinter.displayMessage(Messages.GAME_SAVED);
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.exit(0);
                }else{
                    try{
                        Constants.Direction direction = Constants.Direction.valueOf(input[1]);
                        validAction = (Boolean) Commands.COMMANDS_METHOD_MAP.get(Commands.valueOf(command)).invoke(game, direction, pc);
                    }catch (Exception ex){
                        //TODO: Eren: logging
                        ex.printStackTrace();
                    }
                }
            }
            if(game.hasPlayerLeveledUp()){
                levelUpDialogue(game);
            }
            if(pc.getRemainingActions()>0){
                FancyPrinter.printMap(game);
            }
        }
        for (EnemyCharacter enemyCharacter :
                game.getEnemies()) {
            enemyCharacter.refreshActions();
            while (enemyCharacter.getRemainingActions() > 0){
                if(!game.moveEnemy(enemyCharacter)){
                    boolean validAction = false;
                    while (!validAction){
                        validAction = game.moveEnemyRandomly(enemyCharacter);
                    }
                }
                enemyCharacter.useAction();
            }
        }
        for(int i = 0; i<Constants.ENEMIES_CREATED_PER_TURN;i++){
            game.addARandomEnemy();
        }
        if(turn%5==0){
            for(int i = 0; i<Constants.QUESTS_CREATED_PER_FIVE_TURNS; i++) {
                game.addARandomQuest();
            }
        }
        FancyPrinter.printMap(game);
    }

    /**
     * Runs when the player has leveled up. Allows player
     * to pick a stat and then calls the game object's
     * level up method
     * @param game current game
     */
    private static void levelUpDialogue(Game game) {
        String input = "";
        boolean inputAccepted = false;
        while(!inputAccepted){
            FancyPrinter.displayMessage(Messages.LVL_UP);
            try {
                input = br.readLine();
                input = input.trim().toUpperCase();
                if(Constants.Stat.valueOf(input) != null){
                    inputAccepted = true;
                    game.levelUp(Constants.Stat.valueOf(input));
                }
            } catch (IOException e) {
                FancyPrinter.displayInvalidInputMessage();
            }
        }
    }

    /**
     * Takes a yes or no user input,
     * returns the result as boolean
     * @return true or false
     */
    private static boolean yesNoInput(){
        String input = "";
        while(!input.equals("y") && !input.equals("n")&&!input.equals("yes") && !input.equals("no")){
            FancyPrinter.displayMessage(Messages.YES_OR_NO);
            try {
                input = br.readLine();
                input = input.toLowerCase().trim();
            } catch (IOException e) {
                FancyPrinter.displayInvalidInputMessage();
            }
        }
        if(input.equals("yes") || input.equals("y"))
            return true;
        else
            return false;
    }

    /**
     * Takes an action with a direction as input and validates it.
     * Keeps asking the user until a valid input is given
     * @return An array of two. First element is action, second is direction
     */
    private static String[] actionInput(){
        String input = "";
        boolean inputAccepted = false;
        while(!inputAccepted){
            FancyPrinter.printCommantInputMessage();
            try {
                input = br.readLine();
                input = input.toUpperCase().trim();
                if(input.equals(Commands.SAVE.name())){
                    String[] returnVal = {input};
                    inputAccepted = true;
                    return returnVal;
                }
                String[] inputComponents = input.split(" ");
                if(inputComponents.length == 2){
                    if(Commands.valueOf(inputComponents[0].trim())!=null && Constants.Direction.valueOf(inputComponents[1].trim()) != null){
                        inputAccepted = true;
                        return inputComponents;
                    }
                }
            } catch (Exception e) {
                FancyPrinter.displayInvalidInputMessage();
            }
        }
        return new String[2];
    }
}
