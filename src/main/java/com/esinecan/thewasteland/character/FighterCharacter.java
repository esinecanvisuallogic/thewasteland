package com.esinecan.thewasteland.character;

import com.esinecan.thewasteland.constants.CharacterTypes;
import com.esinecan.thewasteland.constants.CombatStats;
import com.esinecan.thewasteland.constants.Constants;

/**
 * Created by eren on 28.08.2016.
 */

/**
 * A sub class of Character. Any new Character that is going to roam
 * around the map and take actions need to implement this class.
 */
public class FighterCharacter extends Character {
    /**
     * Creates a new fighter character and sets the base stats
     * as specified in CharacterTypes constant FIGHTER_STATS
     * @param characterType Type of the Fighter Character. Can
     *                      only take values from FIGHTERS list
     *                      on Character Type.
     */
    public FighterCharacter(CharacterTypes.CharacterType characterType){
        super(characterType);
        this.setGold(Constants.BASE_GOLD);
        this.setAttackable(true);
        CombatStats combatStats = CharacterTypes.CharacterType.FIGHTER_STATS.get(this.getType());
        this.setDamage(combatStats.getDamage());
        this.setHealth(combatStats.getHealth());
        this.setSpeed(combatStats.getSpeed());
        this.refreshActions();
    }

    /**
     * Hit points of the character's car
     */
    private int health;
    /**
     * Multiplied with 0.01, gives out the number of actions character can take per turn
     */
    private int speed;
    /**
     * Damage per hit
     */
    private int damage;
    /**
     * Used on pit stops for healing
     */
    private int gold;
    /**
     * How many more actions can the character take for remainder
     * of this turn.
     */
    private int remainingActions;

    /**
     * At the beginning of each turn,
     * remaining actions need to be refreshed.
     * This method needs to be called to do so.
     * @return remainin actions for turn.
     */
    public int refreshActions(){
        Double actions = new Double((this.getSpeed() * Constants.SPEED_MODIFIER));
        this.setRemainingActions(actions.intValue());
        return this.getRemainingActions();
    }

    /**
     * After every action performed within a turn,
     * this method needs to be called to decrease
     * the remaining actions.
     * @return new remaining action points
     */
    public int useAction(){
        this.setRemainingActions(this.getRemainingActions() - 1);
        return this.getRemainingActions();
    }

    public int getRemainingActions() {
        return remainingActions;
    }

    public void setRemainingActions(int remainingActions) {
        this.remainingActions = remainingActions;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public int getGold() {
        return gold;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }
}
