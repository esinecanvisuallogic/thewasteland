package com.esinecan.thewasteland.character;

import com.esinecan.thewasteland.constants.CharacterTypes;
import com.esinecan.thewasteland.constants.Constants;

import java.util.Random;

/**
 * Created by eren on 29.08.2016.
 */

/**
 * The character that gives player character a quest, and when the
 * said quest is completed, rewards the player character with gold
 * and experience when reported back to them.
 */
public class QuestGiverCharacter extends NonFighterCharacter{
    /**
     * Constructor. Sets the character type as quest giver
     * Sets a kill count for the quest between 1 and MAX_KILLS_ASKED_BY_QUEST
     * Sets a random type of FIGHTER character that needs to be killed
     * Sets a random Reward between MIN_REWARDS_EARNED_BY_QUEST and
     * MAX_REWARDS_EARNED_BY_QUEST specified in the Constants.
     * Sets the quest complete value as false
     * Sets the quest taken as false
     * Sets currently killed enemies for quest count as zero.
     */
    public QuestGiverCharacter (){
        super(CharacterTypes.CharacterType.QUEST_GIVER);
        Random random = new Random();
        this.setNumberToKill(random.nextInt(Constants.MAX_KILLS_ASKED_BY_QUEST) + 1);
        this.setTypeToKill(CharacterTypes.getRandomFighter());
        this.setReward(random.nextInt(Constants.MAX_REWARDS_EARNED_BY_QUEST - Constants.MIN_REWARDS_EARNED_BY_QUEST) + Constants.MIN_REWARDS_EARNED_BY_QUEST);
        this.setQuestComplete(false);
        this.setQuestTaken(false);
        this.setQuestCounter(0);
    }

    /**
     * How many enemies need to be killed for this quest
     */
    private int numberToKill;
    /**
     * What type of FIGHTERs need to be killed for
     * this quest
     */
    private CharacterTypes.CharacterType typeToKill;
    /**
     * How many golds will be rewarded upon completion
     */
    private int reward;
    /**
     * is this quest taken by the character already
     */
    private boolean questTaken;
    /**
     * is this quest requirements met by the character
     * already
     */
    private boolean questComplete;
    /**
     * How many matching enemies have been killed for
     * this quest.
     */
    private int questCounter;

    public int getNumberToKill() {
        return numberToKill;
    }

    public void setNumberToKill(int numberToKill) {
        this.numberToKill = numberToKill;
    }

    public CharacterTypes.CharacterType getTypeToKill() {
        return typeToKill;
    }

    public void setTypeToKill(CharacterTypes.CharacterType typeToKill) {
        this.typeToKill = typeToKill;
    }

    public int getReward() {
        return reward;
    }

    public void setReward(int reward) {
        this.reward = reward;
    }

    public boolean isQuestTaken() {
        return questTaken;
    }

    public void setQuestTaken(boolean questTaken) {
        this.questTaken = questTaken;
    }

    public boolean isQuestComplete() {
        return questComplete;
    }

    public void setQuestComplete(boolean questComplete) {
        this.questComplete = questComplete;
    }

    public int getQuestCounter() {
        return questCounter;
    }

    public void setQuestCounter(int questCounter) {
        this.questCounter = questCounter;
    }
}
