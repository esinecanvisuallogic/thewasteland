package com.esinecan.thewasteland.character;

import com.esinecan.thewasteland.constants.CharacterTypes;
import com.esinecan.thewasteland.constants.Constants;

import java.util.Random;

/**
 * Created by eren on 28.08.2016.
 */

/**
 * Enemy class that the player will fight over the course of the game
 */
public class EnemyCharacter extends FighterCharacter{

    /**
     * Enemy Constructor. Will be assigned a random name on creation.
     * @param characterType is the type of enemy. War Boy, Imperator, etc
     *                      Different character Types will have different
     *                      base stats.
     */
    public EnemyCharacter (CharacterTypes.CharacterType characterType){
        super(characterType);
        int nameIndex = new Random().nextInt(Constants.NPC_NAME_POOL.length);
        this.setName(Constants.NPC_NAME_POOL[nameIndex]);
    }

    /**
     * A simple method for concatenating enemy's type and name for display during the game
     * @return character's full title. e.g. "Nux the WAR BOY"
     */
    public String getFullTitle(){
        return (this.getName() +" the "+ this.getType()).replace("_", " ");
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;
}
