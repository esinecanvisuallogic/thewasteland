package com.esinecan.thewasteland.character;

import com.esinecan.thewasteland.constants.CharacterTypes;

/**
 * Created by eren on 28.08.2016.
 */

/**
 * This is the character that we move around and play with
 */
public class PlayableCharacter extends FighterCharacter {

    /**
     * Constructor. Creates a new character whose level
     * is 1, who has zero experience and who has a maximum
     * health value depending on their character type
     * @param characterType needs to be one of the fighter
     *                      characters specified in Character
     *                      Types, FIGHTERS
     */
    public PlayableCharacter(CharacterTypes.CharacterType characterType){
        super(characterType);
        this.setLevel(1);
        this.setExperience(0);
        this.setFullHealth(this.getHealth());
    }

    /**
     * Current level of character
     */
    private int level;
    /**
     * Current experience points that the character
     * has earned over the course of game.
     */
    private int experience;
    /**
     * Maximum hit points this character can have
     */
    private int fullHealth;

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public int getFullHealth() {
        return fullHealth;
    }

    public void setFullHealth(int fullHealth) {
        this.fullHealth = fullHealth;
    }
}
