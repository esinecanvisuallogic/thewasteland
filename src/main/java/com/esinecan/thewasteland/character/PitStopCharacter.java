package com.esinecan.thewasteland.character;

import com.esinecan.thewasteland.constants.CharacterTypes;
import com.esinecan.thewasteland.constants.Constants;

import java.util.Random;

/**
 * Created by eren on 29.08.2016.
 */

/**
 * The character type that when playable character interacts with,
 * heals the playable character for a cost.
 */
public class PitStopCharacter extends NonFighterCharacter{

    /**
     * How much gold does the player character needs
     * to pay yo get healed.
     */
    private int cost;
    /**
     * How many hit points will the character be
     * healed in exchange for the cost
     */
    private int healingCapacity;

    /**
     * Constructor. Sets the character's type as pit stop
     * sets a random cost between MIN_PIT_STOP_COST and
     * MAX_PIT_STOP_COST specified in the constants.
     * Similarly, sets a random healing capacity between
     * MIN_PIT_STOP_HEAL and MAX_PIT_STOP_HEAL specified
     * in the constants
     */
    public PitStopCharacter (){
        super(CharacterTypes.CharacterType.PIT_STOP_OWNER);
        Random random = new Random();
        this.setCost(random.nextInt(Constants.MAX_PIT_STOP_COST - Constants.MIN_PIT_STOP_COST) + Constants.MIN_PIT_STOP_COST);
        this.setHealingCapacity(random.nextInt(Constants.MAX_PIT_STOP_HEAL - Constants.MIN_PIT_STOP_HEAL) + Constants.MIN_PIT_STOP_HEAL);
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public int getHealingCapacity() {
        return healingCapacity;
    }

    public void setHealingCapacity(int healingCapacity) {
        this.healingCapacity = healingCapacity;
    }
}
