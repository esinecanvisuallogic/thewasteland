package com.esinecan.thewasteland.character;

import com.esinecan.thewasteland.constants.CharacterTypes;

import java.io.Serializable;

/**
 * Created by eren on 28.08.2016.
 */

/**
 * Base character class. All character classes must implement this.
 */
public class Character implements Serializable{
    public Character(CharacterTypes.CharacterType characterType){
        this.setType(characterType);
    }

    public int getXPosition() {
        return xPosition;
    }

    public void setXPosition(int xPosition) {
        this.xPosition = xPosition;
    }

    public int getYPosition() {
        return yPosition;
    }

    public void setYPosition(int yPosition) {
        this.yPosition = yPosition;
    }

    public boolean isAttackable() {
        return attackable;
    }

    public void setAttackable(boolean attackable) {
        this.attackable = attackable;
    }

    public CharacterTypes.CharacterType getType() {
        return type;
    }

    public void setType(CharacterTypes.CharacterType type) {
        this.type = type;
    }

    /**
     * Character's Type. Imperator, War Boy etc.
     */
    private CharacterTypes.CharacterType type;
    /**
     * x coordinate on the map
     */
    private int xPosition;
    /**
     * x coordinate on the map
     */
    private int yPosition;
    /**
     * npc characters that are not fighters can't be attacked
     */
    private boolean attackable;
}
