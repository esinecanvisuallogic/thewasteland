package com.esinecan.thewasteland.map;

import com.esinecan.thewasteland.character.Character;
import com.esinecan.thewasteland.constants.Constants;

import java.io.Serializable;
import java.util.List;

/**
 * Created by eren on 29.08.2016.
 */

/**
 * The game map object that holds the location of characters
 */
public class GameMap implements Serializable{
    /**
     * Since our game is a 2d game, our map is a 2d map
     */
    private Character[][] gameMap;

    /**
     * Constructor.
     * instantiates an empty game map
     */
    public GameMap(){
        gameMap = new Character[Constants.MAP_HEIGHT][Constants.MAP_WIDTH];
    }

    /**
     * Positions the given character on the map randomly.
     * If the first attempt hits an already occupied coordinate,
     * method keeps trying in a loop until an empty coordinate is found
     * @param character character to be positioned
     */
    public void positionSingleCharacter(Character character){
        boolean settled = false;
        while(!settled){
            Coordinate coordinate = CoordinateCalculator.getRandomCoordinate();
            if(CoordinateCalculator.isCoordinateEmpty(coordinate, this)){
                gameMap[coordinate.y][coordinate.x] = character;
                character.setXPosition(coordinate.x);
                character.setYPosition(coordinate.y);
                settled = true;
            }
        }
    }

    /**
     * A shorthand method for positioning multiple characters.
     * Uses the positionSingleCharacter method
     * @param characters a list of objects that extend Character class
     */
    public void positionCharacters(List<? extends Character> characters) {
        for(Character character: characters){
            positionSingleCharacter(character);
        }
    }
    public Character[][] getGameMap() {
        return gameMap;
    }

    public void setGameMap(Character[][] gameMap) {
        this.gameMap = gameMap;
    }


}
