package com.esinecan.thewasteland.map;

import com.esinecan.thewasteland.character.Character;
import com.esinecan.thewasteland.constants.Constants;

import java.util.Random;

/**
 * Created by eren on 29.08.2016.
 */

/**
 * Statically accesed class to make calculations
 */
public class CoordinateCalculator {
    /**
     * Method that says if the coordinates are within the map
     * @param coordinate object to be checked
     * @return true or false
     */
    public static boolean coordinatesValid(Coordinate coordinate) {
        if(coordinate.x<0 || coordinate.x> Constants.MAP_WIDTH-1){
            return false;
        }else if(coordinate.y<0 || coordinate.y>Constants.MAP_HEIGHT-1) {
            return false;
        }
        return true;
    }

    /**
     * Is there anyone already on the given coordinates
     * @param coordinate object to be checked
     * @param gameMap map to be investigated
     * @return true or false
     */
    public static boolean isCoordinateEmpty(Coordinate coordinate, GameMap gameMap){
        return gameMap.getGameMap()[coordinate.y][coordinate.x] == null;
    }

    /**
     * Returns the character located in the specified coordinates
     * @param coordinate coordinate to be checked
     * @param gameMap map to be investigated
     * @return Character object
     */
    public static Character getCharacterAtCoordinate(Coordinate coordinate, GameMap gameMap){
        return gameMap.getGameMap()[coordinate.y][coordinate.x];
    }

    /**
     * Generates a random coordinate. Usually used for putting a
     * character on the map for the first time
     * @return Coordinates
     */
    public static Coordinate getRandomCoordinate(){
        Random random = new Random();
        Coordinate coordinate = new Coordinate(random.nextInt(Constants.MAP_WIDTH),random.nextInt(Constants.MAP_HEIGHT));
        return coordinate;
    }

    /**
     * Returns the target coordinates of an action with a direction
     * such as ATTACK SOUTH
     * @param direction One of the directions in Constants
     * @param x X coordinate
     * @param y Y coordinate
     * @return a new coordinate object
     */
    public static Coordinate getCoordinateToDirection(Constants.Direction direction, int x, int y){
        int newXPosition = x;
        int newYPosition = y;
        if(direction == Constants.Direction.NORTH){
            newYPosition --;
        }else if(direction == Constants.Direction.SOUTH){
            newYPosition ++;
        }else if(direction == Constants.Direction.WEST){
            newXPosition --;
        }else if(direction == Constants.Direction.EAST){
            newXPosition ++;
        }
        return new Coordinate(newXPosition, newYPosition);
    }
}
