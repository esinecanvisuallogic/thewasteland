package com.esinecan.thewasteland.map;

/**
 * Created by eren on 29.08.2016.
 */

/**
 * Coordinate object used to help with calculations
 */
public class Coordinate {
    /**
     * x coordinate
     */
    public int x;
    /**
     * y coordinate
     */
    public int y;

    /**
     * Constructor
     * @param x
     * @param y
     */
    public Coordinate(int x, int y){
        this.x = x;
        this.y = y;
    }
}
