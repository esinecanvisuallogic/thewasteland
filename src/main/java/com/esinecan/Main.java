package com.esinecan;

import com.esinecan.thewasteland.ui.GameUI;

/**
 * Created by eren on 27.08.2016.
 */
public class Main {
    public static void main(String[] args) {
        GameUI.start();
    }
}
