package com.esinecan.thewasteland.at;

import com.esinecan.thewasteland.character.QuestGiverCharacter;
import com.esinecan.thewasteland.constants.CharacterTypes;
import com.esinecan.thewasteland.constants.Constants;
import junit.framework.TestCase;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.steps.Steps;

import static junit.framework.Assert.assertTrue;

/**
 * Created by eren on 29.08.2016.
 */
public class CreateQuestGiverSteps extends Steps {
    QuestGiverCharacter questGiverCharacter;
    @When("I create a quest giver")
    public void whenICreateAQuestGiver() {
        this.questGiverCharacter = new QuestGiverCharacter();
    }

    @Then("he wants a number of enemies killed between one and max values")
    public void thenHeWantsANumberOfEnemiesKilledBetweenOneAndMaxValues() {
        TestCase.assertTrue(questGiverCharacter.getNumberToKill() >= 1 && questGiverCharacter.getNumberToKill()<= Constants.MAX_KILLS_ASKED_BY_QUEST);
    }

    @Then("he wants a type of enemies killed")
    public void thenHeWantsATypeOfEnemiesKilled() {
        TestCase.assertTrue(CharacterTypes.CharacterType.FIGHTERS.contains(questGiverCharacter.getTypeToKill()));
    }

    @Then("he wants a to give a number of coins upon completion between min and max values")
    public void thenHeWantsAToGiveANumberOfCoinsUponCompletionBetweenMinAndMaxValues() {
        assertTrue(questGiverCharacter.getReward() >= Constants.MIN_REWARDS_EARNED_BY_QUEST && questGiverCharacter.getReward()<= Constants.MAX_REWARDS_EARNED_BY_QUEST);
    }
}
