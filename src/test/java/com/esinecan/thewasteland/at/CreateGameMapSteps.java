package com.esinecan.thewasteland.at;

import com.esinecan.thewasteland.character.Character;
import com.esinecan.thewasteland.character.EnemyCharacter;
import com.esinecan.thewasteland.character.PitStopCharacter;
import com.esinecan.thewasteland.character.QuestGiverCharacter;
import com.esinecan.thewasteland.constants.CharacterTypes;
import com.esinecan.thewasteland.constants.Constants;
import com.esinecan.thewasteland.map.GameMap;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.steps.Steps;

import static junit.framework.Assert.assertNotSame;
import static junit.framework.Assert.assertTrue;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by eren on 29.08.2016.
 */
public class CreateGameMapSteps extends Steps {
    private List<Character> characters;
    private GameMap gameMap;

    @Given("a character list")
    public void givenACharacterList() {
        characters = new ArrayList<>();
        for(int i = 0; i< Constants.INITIAL_ENEMY_COUNT; i++){
            CharacterTypes.CharacterType characterType = CharacterTypes.getRandomFighter();
            EnemyCharacter enemyCharacter = new EnemyCharacter(characterType);
            characters.add(enemyCharacter);
        }
        for(int i = 0; i< Constants.INITIAL_QUEST_COUNT; i++){
            characters.add(new QuestGiverCharacter());
        }
        for(int i = 0; i< Constants.INITIAL_PIT_STOP_COUNT; i++){
            characters.add(new PitStopCharacter());
        }
    }

    @When("I create a game map")
    public void whenICreateAGameMap() {
        gameMap = new GameMap();
    }

    @Then("an empty map of the sizes specified will be created")
    public void thenAnEmptyMapOfTheSizesSpecifiedWillBeCreated() {
        assertEquals(gameMap.getGameMap().length, Constants.MAP_HEIGHT);
        assertEquals(gameMap.getGameMap()[0].length, Constants.MAP_WIDTH);
    }

    @When("I randomly position each character on the map")
    public void whenIRandomlyPositionEachCharacterOnTheMap() {
        gameMap.positionCharacters(characters);
    }

    @Then("every character will be in their assigned position")
    public void thenEveryCharacterWillBeInTheirAssignedPosition() {
        boolean assignedPos = true;
        for(Character character: characters){
            if(character != gameMap.getGameMap()[character.getYPosition()][character.getXPosition()]){
                assignedPos = false;
                break;
            }
        }
        assertTrue(assignedPos);
    }

    @Then("no characters will be in overlapping positions")
    public void thenNoCharactersWillBeInOverlappingPositions() {
        boolean noOverlap = true;
        for(Character character: characters){
            for(Character characterToCompare: characters){
                if(characterToCompare.getYPosition()==character.getYPosition() && characterToCompare.getXPosition()==character.getXPosition() && character!=characterToCompare){
                    noOverlap = false;
                    break;
                }
            }
        }
        assertTrue(noOverlap);
    }
}
