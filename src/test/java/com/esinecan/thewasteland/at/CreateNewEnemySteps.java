package com.esinecan.thewasteland.at;

import com.esinecan.thewasteland.character.EnemyCharacter;
import com.esinecan.thewasteland.constants.CharacterTypes;
import com.esinecan.thewasteland.constants.Constants;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.steps.Steps;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static junit.framework.Assert.assertNotSame;
import static junit.framework.Assert.assertTrue;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;

/**
 * Created by eren on 28.08.2016.
 */
public class CreateNewEnemySteps  extends Steps {
    private EnemyCharacter enemyCharacter;

    @Given("I want to create an enemy")
    public void givenIWantToCreateAnEnemy() {
    }

    @When("an enemy character is created")
    public void whenAnEnemyCharacterIsCreated() {
        CharacterTypes.CharacterType characterType = CharacterTypes.getRandomFighter();
        this.enemyCharacter = new EnemyCharacter(characterType);
    }

    @Then("the enemy character has a type")
    public void thenTheEnemyCharacterHasAType() {
        assertNotNull(this.enemyCharacter.getType());
    }

    @Then("the enemy character has a health")
    public void thenTheEnemyCharacterHasAHealth() {
        assertNotSame(this.enemyCharacter.getHealth(),0);
    }

    @Then("the enemy character has a speed")
    public void thenTheEnemyCharacterHasASpeed() {
        assertNotSame(this.enemyCharacter.getSpeed(),0);
    }

    @Then("the enemy character has a damage")
    public void thenTheEnemyCharacterHasADamage() {
        assertNotSame(this.enemyCharacter.getDamage(),0);
    }

    @Then("the enemy character has 300 golds")
    public void thenTheEnemyCharacterHas300Golds() {
        assertEquals(this.enemyCharacter.getGold(),300);
    }

    @Then("the enemy character has a name from the pool")
    public void thenTheEnemyCharacterHasANameFromThePool() {
        List<String> possibleNames = Arrays.asList(Constants.NPC_NAME_POOL);
        String characterName = this.enemyCharacter.getName();
        assert(possibleNames.contains(characterName));
    }

    @Then("the enemy character will be attackable")
    public void thenTheEnemyCharacterWillBeAttackable() {
        assertTrue(this.enemyCharacter.isAttackable());
    }
}
