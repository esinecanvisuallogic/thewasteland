package com.esinecan.thewasteland.at;

import com.esinecan.thewasteland.Game;
import com.esinecan.thewasteland.character.PitStopCharacter;
import com.esinecan.thewasteland.constants.CharacterTypes;
import com.esinecan.thewasteland.constants.Constants;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.steps.Steps;

/**
 * Created by eren on 29.08.2016.
 */
public class HealPlayerSteps extends Steps {
    private Game game;
    int oldHealth;
    int oldGold;
    @Given("I am not at full health")
    public void givenIAmNotAtFullHealth() {
        game = new Game();
        game.startGame(CharacterTypes.getRandomFighter());
        game.getPlayableCharacter().setHealth(game.getPlayableCharacter().getHealth() - 70);
    }

    @When("I am near a PitStop")
    public void whenIAmNearAPitStop() {
        PitStopCharacter pitStopCharacter = game.getPitStops().get(0);
        game.getPlayableCharacter().setXPosition(pitStopCharacter.getXPosition());
        game.getPlayableCharacter().setYPosition(pitStopCharacter.getYPosition() - 1);
    }

    @When("I do a PitStop action towards its direction")
    public void whenIDoAPitStopActionTowardsItsDirection() {
        oldHealth = game.getPlayableCharacter().getHealth();
        oldGold = game.getPlayableCharacter().getGold();
        game.doPitStop(Constants.Direction.SOUTH, game.getPlayableCharacter());
    }

    @Then("I should have more health")
    public void thenIShouldHaveMoreHealth() {
        assert(game.getPlayableCharacter().getHealth() > oldHealth);
    }

    @Then("I should have less gold")
    public void thenIShouldHaveLessGold() {
        assert(game.getPlayableCharacter().getGold()<oldGold);
    }

    @When("I am at full health")
    public void whenIAmAtFullHealth() {
        game.getPlayableCharacter().setHealth(game.getPlayableCharacter().getFullHealth());
    }

    @Then("I should have same health")
    public void thenIShouldHaveSameHealth() {
        assert (oldHealth == game.getPlayableCharacter().getHealth());
    }

    @Then("I should have same gold")
    public void thenIShouldHaveSameGold() {
        assert (oldGold == game.getPlayableCharacter().getGold());
    }

    @When("I am only a little below full health")
    public void whenIAmOnlyALittleBelowFullHealth() {
        game.getPlayableCharacter().setHealth(game.getPlayableCharacter().getFullHealth()-1);
    }

    @Then("I should have full health")
    public void thenIShouldHaveFullHealth() {
        assert (game.getPlayableCharacter().getFullHealth() == game.getPlayableCharacter().getHealth());
    }
}
