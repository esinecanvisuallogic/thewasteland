package com.esinecan.thewasteland.at;

import com.esinecan.thewasteland.Game;
import com.esinecan.thewasteland.constants.CharacterTypes;
import com.esinecan.thewasteland.constants.Constants;
import com.esinecan.thewasteland.map.Coordinate;
import com.esinecan.thewasteland.map.CoordinateCalculator;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.steps.Steps;

/**
 * Created by eren on 29.08.2016.
 */
public class TakeAQuestSteps extends Steps {
    private Game game;
    private int oldQuestCount;
    private int oldXP;
    private int oldGold;

    @Given("I'm near a quest giver")
    public void givenImNearAQuestGiver() {
        game = new Game();
        game.startGame(CharacterTypes.getRandomFighter());
        game.getGameMap().getGameMap()[12][12] = game.getPlayableCharacter();
        game.getGameMap().getGameMap()[11][12] = game.getEnemies().get(0);
        game.getGameMap().getGameMap()[13][12] = game.getQuestGivers().get(0);
        game.getPlayableCharacter().setXPosition(12);
        game.getPlayableCharacter().setYPosition(12);
        game.getEnemies().get(0).setXPosition(12);
        game.getEnemies().get(0).setYPosition(11);
        game.getQuestGivers().get(0).setXPosition(12);
        game.getQuestGivers().get(0).setYPosition(12);
    }

    @When("I do a quest action towards them")
    public void whenIDoAQuestActionTowardsThem() {
        game.quest(Constants.Direction.SOUTH, game.getPlayableCharacter());
    }

    @Then("they are marked as an active quest")
    public void thenTheyAreMarkedAsAnActiveQuest() {
        assert (game.getQuestGivers().get(0).isQuestTaken());
    }

    @When("I kill their type of enemy")
    public void whenIKillTheirTypeOfEnemy() {
        oldQuestCount = game.getQuestGivers().get(0).getQuestCounter();
        game.getEnemies().get(0).setType(game.getQuestGivers().get(0).getTypeToKill());
        game.getEnemies().get(0).setHealth(1);
        game.attack(Constants.Direction.NORTH, game.getPlayableCharacter());
    }

    @Then("their quest counter is updated")
    public void thenTheirQuestCounterIsUpdated() {
        assert (game.getQuestGivers().get(0).getQuestCounter() > oldQuestCount);
    }

    @When("quest counter reaches their quota")
    public void whenQuestCounterReachesTheirQuota() {
        game.getGameMap().getGameMap()[11][12] = game.getEnemies().get(0);
        game.getEnemies().get(0).setXPosition(12);
        game.getEnemies().get(0).setYPosition(11);
        game.getEnemies().get(0).setType(game.getQuestGivers().get(0).getTypeToKill());
        game.getEnemies().get(0).setHealth(1);
        game.getQuestGivers().get(0).setQuestCounter(0);
        game.getQuestGivers().get(0).setNumberToKill(1);
        game.attack(Constants.Direction.NORTH, game.getPlayableCharacter());
    }

    @Then("they are marked as a complete quest")
    public void thenTheyAreMarkedAsACompleteQuest() {
        assert (game.getQuestGivers().get(0).isQuestComplete());
    }

    @When("I report to them")
    public void whenIReportToThem() {
        oldXP = game.getPlayableCharacter().getExperience();
        oldGold = game.getPlayableCharacter().getGold();
        game.quest(Constants.Direction.SOUTH, game.getPlayableCharacter());
    }

    @Then("I get experience")
    public void thenIGetExperience() {
        assert (game.getPlayableCharacter().getExperience() > oldXP);
    }

    @Then("I get gold")
    public void thenIGetGold() {
        assert (game.getPlayableCharacter().getGold() > oldGold);
    }

    @Then("they disappear")
    public void thenTheyDisappear() {
        Coordinate coordinate = new Coordinate(12,13);
        assert (CoordinateCalculator.getCharacterAtCoordinate(coordinate, game.getGameMap()) == null);
    }
}
