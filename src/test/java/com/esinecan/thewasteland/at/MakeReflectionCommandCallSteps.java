package com.esinecan.thewasteland.at;

import com.esinecan.thewasteland.Game;
import com.esinecan.thewasteland.character.PlayableCharacter;
import com.esinecan.thewasteland.constants.CharacterTypes;
import com.esinecan.thewasteland.constants.Commands;
import com.esinecan.thewasteland.constants.Constants;
import com.esinecan.thewasteland.map.Coordinate;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.steps.Steps;

import java.lang.reflect.InvocationTargetException;
import java.util.Random;

/**
 * Created by eren on 30.08.2016.
 */
public class MakeReflectionCommandCallSteps extends Steps {
    private Game game;
    private PlayableCharacter playableCharacter;
    private Coordinate oldPositions;

    @Given("I have a player")
    public void givenIHaveAPlayerAndADirection() {
        game = new Game();
        game.startGame(CharacterTypes.getRandomFighter());
        playableCharacter = game.getPlayableCharacter();
        oldPositions = new Coordinate(playableCharacter.getXPosition(), playableCharacter.getYPosition());
    }

    @When("I do a generic move call")
    public void whenIDoAGenericMoveCall() {
        boolean hasMoved = false;
        while (!hasMoved){
            int pick = new Random().nextInt(Constants.Direction.values().length);
            Constants.Direction direction = Constants.Direction.values()[pick];
            try {
                hasMoved = (Boolean) Commands.COMMANDS_METHOD_MAP.get(Commands.MOVE).invoke(game, direction, playableCharacter);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }

    @Then("Player will have performed a move in the desired direction")
    public void thenPlayerWillHavePerformedAMoveInTheDesiredDirection() {
        assert (oldPositions.x != playableCharacter.getXPosition() || oldPositions.y != playableCharacter.getYPosition());
    }
}
