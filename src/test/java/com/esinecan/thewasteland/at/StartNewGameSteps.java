package com.esinecan.thewasteland.at;

import com.esinecan.thewasteland.Game;
import com.esinecan.thewasteland.constants.CharacterTypes;
import com.esinecan.thewasteland.constants.Constants;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.steps.Steps;

import static junit.framework.Assert.assertNotSame;
import static junit.framework.Assert.assertTrue;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;

/**
 * Created by eren on 27.08.2016.
 */
public class StartNewGameSteps extends Steps {

    private Game game;

    @Given("I want to start a game")
    public void givenIWantToStartAGame() {
        this.game = new Game();
    }

    @When("the playable character is created")
    public void whenThePlayableCharacterIsCreated() {
        CharacterTypes.CharacterType characterType = CharacterTypes.getRandomFighter();
        game.createPlayableCharacter(characterType);
    }

    @Then("the character has a type")
    public void thenTheCharacterHasAType() {
        assertNotNull(game.getPlayableCharacter().getType());
    }

    @Then("the character has a health")
    public void thenTheCharacterHasAHealth() {
        assertNotSame(game.getPlayableCharacter().getHealth(),0);
    }

    @Then("the character has a speed")
    public void thenTheCharacterHasASpeed() {
        assertNotSame(game.getPlayableCharacter().getSpeed(),0);
    }

    @Then("the character has a damage")
    public void thenTheCharacterHasADamage() {
        assertNotSame(game.getPlayableCharacter().getDamage(),0);
    }

    @Then("the character has 300 golds")
    public void thenTheCharacterHas300Golds() {
        assertEquals(game.getPlayableCharacter().getGold(),300);
    }

    @Then("the character is level 1")
    public void thenTheCharacterIsLevel1() {
        assertEquals(game.getPlayableCharacter().getLevel(), 1);
    }

    @Then("the character has zero experience")
    public void thenTheCharacterHasZeroExperience() {
        assertEquals(game.getPlayableCharacter().getExperience(), 0);
    }

    @Then("the character will be attackable")
    public void thenTheCharacterWillBeAttackable() {
        assertTrue(this.game.getPlayableCharacter().isAttackable());
    }

    @When("a number of enemies are created and stored in the game")
    public void whenANumberOfEnemiesAreCreatedAndStoredInTheGame() {
        this.game.CreateRandomEnemies();
    }

    @Then("there are that number of enemies ready")
    public void thenThereAreThatNumberOfEnemiesReady() {
        assertEquals(this.game.getEnemies().size(), Constants.INITIAL_ENEMY_COUNT);
    }

    @When("a number of quest givers are created and stored in the game")
    public void whenANumberOfQuestGiversAreCreatedAndStoredInTheGame() {
        this.game.CreateQuestGivers();
    }

    @Then("there are that number of quest givers ready")
    public void thenThereAreThatNumberOfQuestGiversReady() {
        assertEquals(this.game.getQuestGivers().size(), Constants.INITIAL_QUEST_COUNT);
    }

    @When("a number of pit stops are created and stored in the game")
    public void whenANumberOfPitStopsAreCreatedAndStoredInTheGame() {
        this.game.CreatePitStops();
    }

    @Then("there are that number of pit stops ready")
    public void thenThereAreThatNumberOfPitStopsReady() {
        assertEquals(this.game.getPitStops().size(), Constants.INITIAL_PIT_STOP_COUNT);
    }

    @When("the game map is initialized")
    public void whenTheGameMapIsInitialized() {
        // PENDING
    }

    @Then("every character will be in their assigned positions")
    public void thenEveryCharacterWillBeInTheirAssignedPositions() {
        // PENDING
    }
}