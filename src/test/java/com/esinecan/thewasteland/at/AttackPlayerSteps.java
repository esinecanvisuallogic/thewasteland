package com.esinecan.thewasteland.at;

import com.esinecan.thewasteland.Game;
import com.esinecan.thewasteland.constants.CharacterTypes;
import com.esinecan.thewasteland.constants.Constants;
import com.esinecan.thewasteland.map.Coordinate;
import com.esinecan.thewasteland.map.CoordinateCalculator;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.steps.Steps;

/**
 * Created by eren on 29.08.2016.
 */
public class AttackPlayerSteps extends Steps {
    private Game game;
    private int oldHealth;
    private int enemyOldHealth;
    private int oldGold;
    private int oldXP;
    private Coordinate oldEnemyPosition;

    @Given("a game is initialized")
    public void givenAGameIsInitialized() {
        game = new Game();
        game.startGame(CharacterTypes.getRandomFighter());
    }

    @When("an enemy is adjacent to me")
    public void whenAnEnemyIsAdjacentToMe() {
        game.getGameMap().getGameMap()[12][12] = game.getPlayableCharacter();
        game.getGameMap().getGameMap()[11][12] = game.getEnemies().get(0);
        game.getPlayableCharacter().setXPosition(12);
        game.getPlayableCharacter().setYPosition(12);
        game.getEnemies().get(0).setXPosition(12);
        game.getEnemies().get(0).setYPosition(11);
        oldEnemyPosition = new Coordinate(12,11);
    }

    @When("they attack me")
    public void whenTheyAttackMe() {
        oldHealth = game.getPlayableCharacter().getHealth();
        assert (game.attack(Constants.Direction.SOUTH, game.getEnemies().get(0)));
    }

    @Then("I should lose health")
    public void thenIShouldLoseHealth() {
        assert (oldHealth>game.getPlayableCharacter().getHealth());
    }

    @When("I attack them")
    public void whenIAttackThem() {
        enemyOldHealth = game.getEnemies().get(0).getHealth();
        assert(game.attack(Constants.Direction.NORTH, game.getPlayableCharacter()));
    }

    @Then("they should lose health")
    public void thenTheyShouldLoseHealth() {
        assert (enemyOldHealth>game.getEnemies().get(0).getHealth());
    }

    @When("I kill an enemy")
    public void whenIKillAnEnemy() {
        oldXP = game.getPlayableCharacter().getExperience();
        oldGold = game.getPlayableCharacter().getGold();
        game.getEnemies().get(0).setHealth(1);
        assert (game.attack(Constants.Direction.NORTH, game.getPlayableCharacter()));
    }

    @Then("I should get experience")
    public void thenIShouldGetExperience() {
        assert (oldXP<game.getPlayableCharacter().getExperience());
    }

    @Then("I should get gold")
    public void thenIShouldGetGold() {
        assert (oldGold<game.getPlayableCharacter().getGold());
    }

    @Then("they should disappear")
    public void thenTheyShouldDisappear() {
        assert (CoordinateCalculator.getCharacterAtCoordinate(oldEnemyPosition, game.getGameMap())==null);
    }
}
