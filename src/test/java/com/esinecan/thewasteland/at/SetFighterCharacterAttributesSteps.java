package com.esinecan.thewasteland.at;

import com.esinecan.thewasteland.character.FighterCharacter;
import com.esinecan.thewasteland.constants.CharacterTypes;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.steps.Steps;

import java.util.Random;

import static junit.framework.Assert.assertNotSame;
import static junit.framework.Assert.assertTrue;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;

/**
 * Created by eren on 28.08.2016.
 */
public class SetFighterCharacterAttributesSteps extends Steps {
    private FighterCharacter fighterCharacter;

    @Given("I have a fighter character")
    public void givenIHaveAFighterCharacter() {

    }

    @When("I set its attributes")
    public void whenISetItsAttributes() {
        CharacterTypes.CharacterType characterType = CharacterTypes.getRandomFighter();
        fighterCharacter = new FighterCharacter(characterType);
    }

    @Then("the fighter character has a type")
    public void thenTheFighterCharacterHasAType() {
        assertNotNull(this.fighterCharacter.getType());
    }

    @Then("the fighter character has a health")
    public void thenTheFighterCharacterHasAHealth() {
        assertNotSame(this.fighterCharacter.getHealth(),0);
    }

    @Then("the fighter character has a speed")
    public void thenTheFighterCharacterHasASpeed() {
        assertNotSame(this.fighterCharacter.getSpeed(),0);
    }

    @Then("the fighter character has a damage")
    public void thenTheFighterCharacterHasADamage() {
        assertNotSame(this.fighterCharacter.getDamage(),0);
    }

    @Then("the fighter character has 300 golds")
    public void thenTheFighterCharacterHas300Golds() {
        assertEquals(this.fighterCharacter.getGold(),300);
    }

    @Then("the fighter character will be attackable")
    public void thenTheFighterCharacterWillBeAttackable() {
        assertTrue(this.fighterCharacter.isAttackable());
    }
}
