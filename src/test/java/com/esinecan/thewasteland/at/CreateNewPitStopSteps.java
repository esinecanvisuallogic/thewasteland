package com.esinecan.thewasteland.at;

import com.esinecan.thewasteland.character.PitStopCharacter;
import com.esinecan.thewasteland.constants.Constants;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.steps.Steps;

import static junit.framework.Assert.assertTrue;

/**
 * Created by eren on 29.08.2016.
 */
public class CreateNewPitStopSteps extends Steps {
    PitStopCharacter pitStopCharacter;

    @When("I create a pit stop")
    public void whenICreateAPitStop() {
        pitStopCharacter = new PitStopCharacter();
    }

    @Then("the pit stop has a cost of healing between min and max values")
    public void thenThePitStopHasACostOfHealingBetweenMinAndMaxValues() {
        assertTrue(pitStopCharacter.getCost() >= Constants.MIN_PIT_STOP_COST && pitStopCharacter.getCost()<= Constants.MAX_PIT_STOP_COST);
    }

    @Then("the pit stop has a capacity of healing between min and max values")
    public void thenThePitStopHasACapacityOfHealingBetweenMinAndMaxValues() {
        assertTrue(pitStopCharacter.getHealingCapacity() >= Constants.MIN_PIT_STOP_HEAL && pitStopCharacter.getHealingCapacity()<= Constants.MAX_PIT_STOP_HEAL);
    }

}
