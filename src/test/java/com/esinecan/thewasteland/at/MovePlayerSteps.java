package com.esinecan.thewasteland.at;

import com.esinecan.thewasteland.Game;
import com.esinecan.thewasteland.character.EnemyCharacter;
import com.esinecan.thewasteland.constants.CharacterTypes;
import com.esinecan.thewasteland.constants.Constants;
import com.esinecan.thewasteland.map.GameMap;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.steps.Steps;

import java.util.Random;

import static org.junit.Assert.assertTrue;

/**
 * Created by eren on 29.08.2016.
 */
public class MovePlayerSteps extends Steps {
    private Game game;
    private int oldX;
    private int oldY;
    private boolean lastMoveResult;

    @Given("I initialize a new game")
    public void givenIInitializeANewGame() {
        game = new Game();
        game.startGame(CharacterTypes.getRandomFighter());
    }

    @When("I move")
    public void whenIMove() {
        boolean hasMoved = false;
        oldX = game.getPlayableCharacter().getXPosition();
        oldY = game.getPlayableCharacter().getYPosition();
        while(!hasMoved){
            int pick = new Random().nextInt(Constants.Direction.values().length);
            Constants.Direction direction = Constants.Direction.values()[pick];
            hasMoved = game.move(direction, game.getPlayableCharacter());
        }
    }

    @Then("I should change position in the desired direction")
    public void thenIShouldChangePositionInTheDesiredDirection() {
        assertTrue(oldX != game.getPlayableCharacter().getXPosition() || oldY != game.getPlayableCharacter().getYPosition());
    }

    @When("I move into an npc")
    public void whenIMoveIntoAnNpc() {
        EnemyCharacter enemyCharacter = game.getEnemies().get(0);
        game.getPlayableCharacter().setXPosition(enemyCharacter.getXPosition());
        game.getPlayableCharacter().setYPosition(enemyCharacter.getYPosition()-1);
        lastMoveResult = game.move(Constants.Direction.SOUTH, game.getPlayableCharacter());
    }


    @When("I move out of map to the south")
    public void whenIMoveOutOfMapToTheSouth() {
        game.getPlayableCharacter().setYPosition(Constants.MAP_HEIGHT-1);
        lastMoveResult = game.move(Constants.Direction.SOUTH, game.getPlayableCharacter());
    }

    @When("I move out of map to the north")
    public void whenIMoveOutOfMapToTheNorth() {
        game.getPlayableCharacter().setYPosition(0);
        lastMoveResult = game.move(Constants.Direction.NORTH, game.getPlayableCharacter());
    }

    @When("I move out of map to the east")
    public void whenIMoveOutOfMapToTheEast() {
        game.getPlayableCharacter().setXPosition(Constants.MAP_WIDTH-1);
        lastMoveResult = game.move(Constants.Direction.EAST, game.getPlayableCharacter());
    }

    @When("I move out of map to the west")
    public void whenIMoveOutOfMapToTheWest() {
        game.getPlayableCharacter().setXPosition(0);
        lastMoveResult = game.move(Constants.Direction.WEST, game.getPlayableCharacter());
    }

    @Then("I should not be able to move")
    public void thenIShouldNotBeAbleToMove() {
        assertTrue(!lastMoveResult);
    }

    @When("my enemies are close to me")
    public void whenMyEnemiesAreCloseToMe() {
        int myX = 15;
        int myY = 15;
        int enemyX = 11;
        int enemyY = 12;
        game.setGameMap(new GameMap());
        game.getPlayableCharacter().setXPosition(myX);
        game.getPlayableCharacter().setYPosition(myY);
        game.getEnemies().get(0).setYPosition(enemyY);
        game.getEnemies().get(0).setXPosition(enemyX);
    }

    @Then("they should come to me")
    public void thenTheyShouldComeToMe() {
        EnemyCharacter enemyCharacter = game.getEnemies().get(0);
        int xDifferenceOld = Math.abs(enemyCharacter.getXPosition() - game.getPlayableCharacter().getXPosition());
        int yDifferenceOld = Math.abs(enemyCharacter.getYPosition() - game.getPlayableCharacter().getYPosition());
        game.moveEnemy(game.getEnemies().get(0));
        int xDifferenceNew = Math.abs(enemyCharacter.getXPosition() - game.getPlayableCharacter().getXPosition());
        int yDifferenceNew = Math.abs(enemyCharacter.getYPosition() - game.getPlayableCharacter().getYPosition());
        assertTrue((xDifferenceNew < xDifferenceOld) || (yDifferenceNew < yDifferenceOld));
    }

    @When("my enemies are not close to me")
    public void whenMyEnemiesAreNotCloseToMe() {
        int myX = 20;
        int myY = 20;
        int enemyX = 11;
        int enemyY = 12;
        game.setGameMap(new GameMap());
        game.getPlayableCharacter().setXPosition(myX);
        game.getPlayableCharacter().setYPosition(myY);
        game.getEnemies().get(0).setYPosition(enemyY);
        game.getEnemies().get(0).setXPosition(enemyX);
    }

    @Then("they should move randomly")
    public void thenTheyShouldMoveRandomly() {
        EnemyCharacter enemyCharacter = game.getEnemies().get(0);
        int oldX = enemyCharacter.getXPosition();
        int oldY = enemyCharacter.getYPosition();
        game.moveEnemy(enemyCharacter);
        assertTrue((oldX != enemyCharacter.getXPosition()) || (oldY != enemyCharacter.getYPosition()));
    }
}
