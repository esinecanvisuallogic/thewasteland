package com.esinecan.thewasteland.at;

import com.esinecan.thewasteland.Game;
import com.esinecan.thewasteland.constants.CharacterTypes;
import com.esinecan.thewasteland.constants.Constants;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.steps.Steps;

import java.util.Random;

/**
 * Created by eren on 30.08.2016.
 */
public class LevelUpSteps extends Steps {
    private Game game;
    private int oldLevel;
    private Constants.Stat chosenStat;
    private int chosenStatOldValue;

    @Given("I'm close to leveling up")
    public void givenImCloseToLevelingUp() {
        game = new Game();
        game.startGame(CharacterTypes.getRandomFighter());
        game.getPlayableCharacter().setExperience(Constants.XP_REQUIRED_PER_LEVEL - 1);
        oldLevel = game.getPlayableCharacter().getLevel();
        int pick = new Random().nextInt(Constants.Stat.values().length);
        chosenStat = Constants.Stat.values()[pick];
        if(chosenStat == Constants.Stat.HEALTH){
            chosenStatOldValue = game.getPlayableCharacter().getHealth();
        }
        else if(chosenStat == Constants.Stat.SPEED){
            chosenStatOldValue = game.getPlayableCharacter().getSpeed();
        }
        else if(chosenStat == Constants.Stat.DAMAGE){
            chosenStatOldValue = game.getPlayableCharacter().getDamage();
        }
    }

    @When("I get more experience and level up")
    public void whenIGetMoreExperienceAndLevelUp() {
        game.getPlayableCharacter().setExperience(game.getPlayableCharacter().getExperience() + 1);
        if(game.hasPlayerLeveledUp()){
            game.levelUp(chosenStat);
        }
    }

    @Then("My level increases by one")
    public void thenMyLevelIncreasesByOne() {
        assert (game.getPlayableCharacter().getLevel() == oldLevel+1);
    }

    @Then("One of my stats increases by a fixed amount")
    public void thenOneOfMyStatsIncreasesByAFixedAmount() {
        int chosenStatNewValue = 0;
        if(chosenStat == Constants.Stat.HEALTH){
            chosenStatNewValue = game.getPlayableCharacter().getHealth();
        }
        else if(chosenStat == Constants.Stat.SPEED){
            chosenStatNewValue = game.getPlayableCharacter().getSpeed();
        }
        else if(chosenStat == Constants.Stat.DAMAGE){
            chosenStatNewValue = game.getPlayableCharacter().getDamage();
        }
        assert (chosenStatNewValue - chosenStatOldValue == Constants.SKILL_POINTS_EARNED_PER_LEVEL);
    }
}
