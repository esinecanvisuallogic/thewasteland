Meta:

Narrative:
We need a map as part of a game's initialization

Scenario: Creating a game map
Given a character list
When I create a game map
Then an empty map of the sizes specified will be created
When I randomly position each character on the map
Then every character will be in their assigned position
Then no characters will be in overlapping positions