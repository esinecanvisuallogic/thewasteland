Meta:

Narrative:
Earning enough experience makes me level up.

Scenario: scenario description
Given I'm close to leveling up
When I get more experience and level up
Then My level increases by one
Then One of my stats increases by a fixed amount