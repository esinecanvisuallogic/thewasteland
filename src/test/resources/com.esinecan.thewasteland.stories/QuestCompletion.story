Meta:

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal

Scenario: scenario description
Given I'm near a Quest Giver
When I take that quest
When I complete its requirements
When I report back
Then I should be rewarded
Then Quest Giver should disappear