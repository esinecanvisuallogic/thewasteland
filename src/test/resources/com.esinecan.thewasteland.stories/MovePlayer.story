Meta:

Narrative:
Movement of player and NPCs.
Enemies will chase you if you're close enough

Scenario: Player movement
Given I initialize a new game
When I move
Then I should change position in the desired direction
When I move into an npc
Then I should not be able to move
When I move out of map to the south
Then I should not be able to move
When I move out of map to the north
Then I should not be able to move
When I move out of map to the east
Then I should not be able to move
When I move out of map to the west
Then I should not be able to move
When my enemies are close to me
Then they should come to me
When my enemies are not close to me
Then they should move randomly
