Meta:

Narrative:
I want to test enemy creation using this story
to see if we can add an enemy properly

Scenario: Creating a single enemy
Given I want to create an enemy
When an enemy character is created
Then the enemy character has a type
Then the enemy character has a health
Then the enemy character has a speed
Then the enemy character has a damage
Then the enemy character has 300 golds
Then the enemy character has a name from the pool
Then the enemy character will be attackable