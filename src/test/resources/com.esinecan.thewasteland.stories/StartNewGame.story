Meta:

Narrative:
As a wastelander, I need to choose
from 3 types of characters,
a map needs to be created
and my allies and enemies need
to be put on the map

Scenario: Initializing the game
Given I want to start a game
When the playable character is created
Then the character has a type
Then the character has a health
Then the character has a speed
Then the character has a damage
Then the character has 300 golds
Then the character will be attackable
Then the character is level 1
Then the character has zero experience
When a number of enemies are created and stored in the game
Then there are that number of enemies ready
When a number of quest givers are created and stored in the game
Then there are that number of quest givers ready
When a number of pit stops are created and stored in the game
Then there are that number of pit stops ready
When the game map is initialized
Then every character will be in their assigned positions