Meta:

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal

Scenario: scenario description
Given I'm near a quest giver
When I do a quest action towards them
Then they are marked as an active quest
When I kill their type of enemy
Then their quest counter is updated
When quest counter reaches their quota
Then they are marked as a complete quest
When I report to them
Then I get experience
Then I get gold
Then they disappear