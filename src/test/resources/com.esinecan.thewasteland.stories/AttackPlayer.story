Meta:

Narrative:
Since bullets are sparse, attacking
an enemy is only possible when adjacent to them

Scenario: Getting in combat
Given a game is initialized
When an enemy is adjacent to me
When they attack me
Then I should lose health
When I attack them
Then they should lose health
When I kill an enemy
Then I should get experience
Then I should get gold
Then they should disappear