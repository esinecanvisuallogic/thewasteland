Meta:

Narrative:
Pit stops are where we can heal
in exchange for gold

Scenario: Creating a pit stop
When I create a pit stop
Then the pit stop has a cost of healing between min and max values
Then the pit stop has a capacity of healing between min and max values