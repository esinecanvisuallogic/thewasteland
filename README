The Wasteland
=============

Being a fan of post apocalyptic games and movies such as Fallout and Mad Max Franchises, I've decided to build a BDD
RPG in which your character is differentiated by their car. You roam the wasteland, fight, level up, find quests, do pit stops
to survive.

Gameplay
-------
In the beginning you're asked whether you want to resume your current game or start a new game. If you choose resume
but no games to load are found, you start a new game.

Then you pick a character class by typing its name.

0. Imperators are heavy hitters, high damage, low speed
0. War Boys are berserkers, heavy damage, low health
0. Road Warriors are speedsters, high speed, low damage

Speed decides how many actions you can take per turn. You can make your speed/100 moves every turn.
The other two stats are health and damage.
Commands comprise of an action and a direction. Move west, attack south, etc.

An "INSPECT" action that does not consume action points has been added.

NPC types are:
0. Purple P's are pit stops that will heal you for money. You go near them and run PITSTOP WEST (or some other direction) to interact
0. Enemies are red E's. They have names from a big post apocalyptic name collection, make sure to check them out. They are similar to player character. They will chase you if you get close. Interact with Attack command.
0. Q stands for quest. They will ask you to kill a number of certain type of enemy. You'll be rewarded with XP and Gold. They change color depending on status. Yellow if you do not interact. Blue if you take the quest but do not meet the requirements. Green if you're ready to collect. Interact with Quest Command. First quest command will get you the mission, once they turn green, second quest command will get you the reward.

Every 5 turns a new quest will come up. Every turn a new enemy shows up. These can be configured from the Constants file.

Base stats of character classes can be adjusted from the CharacterTypes file

How to build
-----------
* To build the project, run: bash gradlew build
* To build the project while looking at some code coverage beauty: bash gradlew build --info

at the project root directory

How to run
-----------
* To run the project, run: java -jar ./build/libs/TheWasteland-1.0.jar inside the project directory
at the project root directory

How to extend
-----------
* Create new character types at CharacterTypes.java. Most of the functionality should be implementeed on new character class itself
* To create new actions and commands, edit Game.java and GameUI.java respectively
* BDD examples are in the test folder. They could serve as guidelines as well.
